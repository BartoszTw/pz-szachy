export class WebSocketClient{
    constructor() {
        this.connection = new WebSocket('ws://localhost:8000/ws/gameroom/' + window.location.pathname.split('/').pop() + '/')
        this._messages = []
        this._moves = []

        this.connection.onopen = (event) => {
            console.log("Successfully connected to the websocket server...")
        }
      
        this.connection.onmessage = (event) => {
            var message = JSON.parse(event.data)
            if(message.type == "chat_message"){
                this._messages.push(message);
            }else if(message.type == "piece_move" || message.type == "piece_move_capture" || message.type == "piece_move_castling"){
                this._moves.push(message);
            }
        }
    }

    send(message){
        this.connection.send(message)
    }

    getMessages(){
        return this._messages
    }

    getMoves(){
        return this._moves
    }
}