import axios from 'axios'

const HOST = "http://localhost:8000"
const DEBUG = false;

const getGameRooms = async (token) => {
    return await requestGet('/api/gamerooms/', {"Authorization": "Token "+ token})
}

const getGameRoom = async (token, roomId) => {
    return await requestGet('/api/gamerooms/'+roomId, {"Authorization": "Token "+ token})
}

const registerGameRoom = async (token, _name, _description) => {
    return await requestPost('/api/gamerooms/', JSON.stringify({
        name: _name,
        description: _description
    }),
    {"Authorization": "Token "+ token})
}

const getUser = async (token) => {
    return await requestGet('/auth/users/me/', {"Authorization": "Token "+ token})
}

const getProfiles = async (token) => {
    return await requestGet('/api/profile', {"Authorization": "Token "+ token})
}

const updateProfile = async (token, profileId, _country, _city, _about_me) => {
    return await requestPut('/api/profile/'+profileId+"/", JSON.stringify({
        country: _country,
        city: _city,
        about_me: _about_me
    }),
    {"Authorization": "Token "+ token})
}
const updateAvatar = async (token, _df, _header) => {
    return await requestPutAlt('/api/avatar/', _df,
    Object.assign({"Authorization": "Token "+ token}, _header))
}

const getProfile = async (token, profileId) => {
    return await requestGet('/api/profile/'+profileId, {"Authorization": "Token "+ token})
}

const logout = async (token) => {
    return await requestPost('/auth/token/logout/', {"Authorization": "Token "+ token})
}

const login = async (_username, _password) => {
    return await requestPost('/auth/token/login/',
        JSON.stringify({
            username: _username,
            password: _password
        })
    )
}

const register = async (_username, _email, _password) => {
    return await requestPost('/auth/users/', 
        JSON.stringify({
            username: _username,
            email: _email,
            password: _password,
            re_password: _password
        })
    )
}

const joinRoom = async (token, roomId) => {
    return await requestPatch('/api/gamerooms/'+roomId+'/join/', {}, {"Authorization": "Token "+ token})
}

const setColor = async (token, roomId, color) => {
    var body = {"assignColor": color}
    return await requestPatch('/api/gamerooms/'+roomId+'/setColor/', body, {"Authorization": "Token "+ token})
}

const requestPost = async (path = undefined, body = undefined, _headers = undefined) => {
    var payload = undefined
    await axios.post(HOST + path, body, {headers: Object.assign({"Content-Type": "application/json"}, _headers)})
    .then(function (response) {
        if(DEBUG) console.log(response)
        payload = response.data;
    })
    .catch(function (error) {
        if(DEBUG) console.log(error);
        payload = error.response.data;
    });
    return payload
}

const requestGet = async (path = undefined, _headers = undefined) => {
    var payload = undefined
    await axios.get(HOST + path, {headers: Object.assign({"Accept": "application/json"}, _headers)})
    .then(function (response) {
        if(DEBUG) console.log(response)
        payload = response.data;
    })
    .catch(function (error) {
        if(DEBUG) console.log(error);
        payload = error.response.data;
    });
    return payload
}

const requestPatch = async (path = undefined, body = undefined, _headers = undefined) => {
    var payload = undefined
    await axios.patch(HOST + path, body, {headers: Object.assign({"Content-Type": "application/json"}, _headers)})
    .then(function (response) {
        if(DEBUG) console.log(response)
        payload = response.data;
    })
    .catch(function (error) {
        if(DEBUG) console.log(error);
        payload = error.response.data;
    });
    return payload
}

const requestPut = async (path = undefined, body = undefined, _headers = undefined) => {
    var payload = undefined
    await axios.put(HOST + path, body, {headers: Object.assign({"Content-Type": "application/json"}, _headers)})
    .then(function (response) {
        if(DEBUG) console.log(response)
        payload = response.data;
    })
    .catch(function (error) {
        if(DEBUG) console.log(error);
        payload = error.response.data;
    });
    return payload
}

const requestPutAlt = async (path = undefined, body = undefined, _headers = undefined) => {
    var payload = undefined
    await axios.put(HOST + path, body, {headers: _headers})
    .then(function (response) {
        if(DEBUG) console.log(response)
        payload = response.data;
    })
    .catch(function (error) {
        if(DEBUG) console.log(error);
        payload = error.response.data;
    });
    return payload
}

export {
    login,
    register,
    logout,
    getUser,
    getGameRooms,
    getGameRoom,
    registerGameRoom,
    getProfile,
    joinRoom,
    setColor,
    getProfiles,
    updateProfile,
    updateAvatar
}