import { createRouter, createWebHistory } from 'vue-router'
import MainView from '../views/MainView.vue'
import LoginView from '../views/LoginView.vue'
import LogoutView from '../views/LogoutView.vue'
import RegisterView from '../views/RegisterView.vue'
import GameView from '../views/GameView.vue'
import ProfileView from '../views/ProfileView.vue'
import ProfileUpdateView from '../views/ProfileUpdateView.vue'
import GameRoomsView from '../views/GameRoomsView.vue'
import GamemodesView from '../views/GamemodesView.vue'
import RankingView from '../views/RankingView.vue'
import MatchHistoryView from '../views/MatchHistoryView.vue'
import NotFoundView from '../views/NotFoundView.vue'
import StatisticsView from '../views/StatisticsView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'main',
      redirect: '/login',
      component: MainView
    },
    {
      path: '/login',
      name: 'login',
      component: LoginView
    },
    {
      path: '/register',
      name: 'register',
      component: RegisterView
    },
    {
      path: '/logout',
      name: 'logout',
      component: LogoutView
    },
    {
      path: '/gamemodes',
      name: 'gamemodes',
      component: GamemodesView
    },
    {
      path: '/profile',
      name: 'profile',
      component: ProfileView
    },
    {
      path: '/settings',
      name: 'settings',
      component: ProfileUpdateView
    },
    {
      path: '/ranking',
      name: 'ranking',
      component: RankingView
    },
    {
      path: '/gamemode/versus/gameroom/:roomid',
      name: 'gameroom',
      component: GameView,
      props: true
    },
    {
      path: '/history',
      name: 'history',
      component: MatchHistoryView
    },
    {
      path: '/stats',
      name: 'stats',
      component: StatisticsView
    },
    {
      path: '/gamemode/versus/gamerooms',
      name: 'gamerooms',
      component: GameRoomsView
    },
    { 
      path: "/:catchAll(.*)", 
      component: NotFoundView 
    }
  ]
})

export default router