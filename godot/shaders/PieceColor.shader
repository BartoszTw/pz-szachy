shader_type canvas_item;

uniform vec4 piece_color1: hint_color = vec4(1.0);
uniform vec4 piece_color2: hint_color = vec4(1.0);
uniform vec4 piece_color3: hint_color = vec4(1.0);
uniform vec4 piece_color4: hint_color = vec4(1.0);

void fragment() {
	vec4 piece_mask_color1 = vec4(1.0, 0.0, 0.0, 1.0);
	vec4 piece_mask_color2 = vec4(0.0, 1.0, 0.0, 1.0);
	vec4 piece_mask_color3 = vec4(0.0, 0.0, 1.0, 1.0);
	vec4 piece_mask_color4 = vec4(1.0, 1.0, 1.0, 1.0);
	float tolerance = 0.0;
	float transparency = 1.0;
	vec4 texture_color = texture(TEXTURE, UV);
	vec3 color = texture_color.rgb;
	
	float alpha = texture_color.a;
	float piece_mask_length1 = length(piece_mask_color1.rgb);
	float piece_mask_length2 = length(piece_mask_color2.rgb);
	float piece_mask_length3 = length(piece_mask_color3.rgb);
	float piece_mask_length4 = length(piece_mask_color4.rgb);
	float color_length = length(color);
	
	vec3 piece_mask_normal1 = piece_mask_color1.rgb / piece_mask_length1 * color_length;
	vec3 piece_mask_normal2 = piece_mask_color2.rgb / piece_mask_length2 * color_length;
	vec3 piece_mask_normal3 = piece_mask_color3.rgb / piece_mask_length3 * color_length;
	vec3 piece_mask_normal4 = piece_mask_color4.rgb / piece_mask_length4 * color_length;
	
	vec3 piece_color_normal1 = piece_color1.rgb / piece_mask_length1 * color_length;
	vec3 piece_color_normal2 = piece_color2.rgb / piece_mask_length2 * color_length;
	vec3 piece_color_normal3 = piece_color3.rgb / piece_mask_length3 * color_length;
	vec3 piece_color_normal4 = piece_color4.rgb / piece_mask_length4 * color_length;
	
	float piece_dist1 = distance(color, piece_mask_normal1);
	float piece_dist2 = distance(color, piece_mask_normal2);
	float piece_dist3 = distance(color, piece_mask_normal3);
	float piece_dist4 = distance(color, piece_mask_normal4);
	
	vec3 color1 = mix(piece_color_normal1, color, tolerance);
	vec3 color2 = mix(piece_color_normal2, color, tolerance);
	vec3 color3 = mix(piece_color_normal3, color, tolerance);
	vec3 color4 = mix(piece_color_normal4, color, tolerance);
	
	if(piece_dist1 < piece_dist2){  
		if(piece_dist1 < piece_dist3){
			if(piece_dist1 < piece_dist4){
				color = color1;
			}else{
				color = color4;
			}
		}else{  
			if(piece_dist3 < piece_dist4){
				color = color3;
			}else{
				color = color4;
			}
		}
	}
	else{ 
		if(piece_dist2 < piece_dist3){
			if(piece_dist2 < piece_dist4){
				color = color2;
			}else{
				color = color4;
			}
		}else{ 
			if(piece_dist3 < piece_dist4){
				color = color3;
			}else{
				color = color4;
			}
		}
	}
	
	COLOR = vec4(color, alpha * transparency);
}