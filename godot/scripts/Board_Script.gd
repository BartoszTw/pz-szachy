extends Node2D

export var BOARD_SIZE_X = 8
export var BOARD_SIZE_Y = 8

onready var tilemap = GlobalTileMap
onready var camera = $Camera2D
onready var pieces = $YSort

#	GameMode must be received from JS
const GAME_MODE = "Clasic"

func _ready():
	setup_board()	
	for x in range(8):
		for y in [0,1,6,7]:
			var p = Scenes.PIECE.instance()
			p.position = Piece.origin_point + Vector2(x,y+1) * 80		#location on Board
			# TODO: uwzględnienić spektatora
			if Global.player_side=="white":
				p.color_pattern = "BLACK" if (y<2) else "WHITE"
			else:
				p.color_pattern = "WHITE" if (y<2) else "BLACK"
			if(y==0 or y == 7):
				#	w zależności od JS
				#	do zmiany
				p.piece_type = pice_type_for_init(GAME_MODE, x, p.color_pattern)
			p.pos = Vector2(x, y)
			p.orientation = 0 if (y<2) else 1
			pieces.add_child(p)
	
func setup_board():
	for x in range(BOARD_SIZE_X):
		for y in range(BOARD_SIZE_Y):
			draw_tile(x, y)
	var tile_map_size = tilemap.get_used_rect().end
	var tile_map_center = (tile_map_size - Vector2(0, 2)) * 40 
	camera.position = tile_map_center

func draw_tile(x, y):
	var tile = (x%2 + y%2)%2
	tilemap.set_cell(x, y, tile) 
	tilemap.set_cell(x, y + 1, tile + 2) 
	
func pice_type_for_init(gameMode, pos, color):
	if("Clasic"):	#White
		match (pos):
			0, 7:
				return 1
			1, 6:
				return 2
			2, 5:
				return 3
			3:
				return 4
			4:
				return 5
			_:
				return 0
	else:
		return 0

