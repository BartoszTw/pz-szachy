extends CanvasLayer

onready var sprite_rook = $Panel/TextureButton/Sprite
onready var sprite_knight = $Panel/TextureButton2/Sprite
onready var sprite_bishop = $Panel/TextureButton3/Sprite
onready var sprite_queen = $Panel/TextureButton4/Sprite
onready var panel = $Panel
onready var status_panel = $StatusPanel
onready var status_panel_label = $StatusPanel/Label
onready var piece_for_promotion = null


func _ready():
	#var color = Palette.PIECE_COLOR[Global.player_side.to_upper()]
	var color = Palette.PIECE_COLOR["GOLD"]
	sprite_rook.material.set_shader_param("piece_color1", Color(color[0]))
	sprite_rook.material.set_shader_param("piece_color2", Color(color[1]))
	sprite_rook.material.set_shader_param("piece_color3", Color(color[2]))
	sprite_rook.material.set_shader_param("piece_color4", Color(color[3]))

	sprite_knight.material.set_shader_param("piece_color1", Color(color[0]))
	sprite_knight.material.set_shader_param("piece_color2", Color(color[1]))
	sprite_knight.material.set_shader_param("piece_color3", Color(color[2]))
	sprite_knight.material.set_shader_param("piece_color4", Color(color[3]))
	
	sprite_bishop.material.set_shader_param("piece_color1", Color(color[0]))
	sprite_bishop.material.set_shader_param("piece_color2", Color(color[1]))
	sprite_bishop.material.set_shader_param("piece_color3", Color(color[2]))
	sprite_bishop.material.set_shader_param("piece_color4", Color(color[3]))
	
	sprite_queen.material.set_shader_param("piece_color1", Color(color[0]))
	sprite_queen.material.set_shader_param("piece_color2", Color(color[1]))
	sprite_queen.material.set_shader_param("piece_color3", Color(color[2]))
	sprite_queen.material.set_shader_param("piece_color4", Color(color[3]))	

	panel.visible = false
	
func _on_FigureViewButton_pressed():
	Global.piece_as_icons = false
	update_pieces()

func _on_IconViewButton_pressed():
	Global.piece_as_icons = true
	update_pieces()

func update_pieces():
	for piece in get_tree().get_nodes_in_group("pieces"):
		piece.update_state()

func _on_rook_pressed():
	piece_for_promotion.piece_type = 1
	piece_for_promotion.color_pattern = "GOLD"
	piece_for_promotion.update_state()
	piece_for_promotion.color_pattern = Global.player_side.to_upper()
	
	Client.send({
		'type':'promotion',
		'new_pos':PossibleMoves.toPosition(piece_for_promotion.old_position),
		'piece_type':1,
		'side':Global.player_side
		})
	
	panel.visible = false
	piece_for_promotion = null
	PossibleMoves.check_for_Check(get_tree().get_nodes_in_group("pieces"))
	pass # Replace with function body.


func _on_knight_pressed():
	piece_for_promotion.piece_type = 2
	piece_for_promotion.color_pattern = "GOLD"
	piece_for_promotion.update_state()
	piece_for_promotion.color_pattern = Global.player_side.to_upper()
	
	Client.send({
		'type':'promotion',
		'new_pos':PossibleMoves.toPosition(piece_for_promotion.old_position),
		'piece_type':2,
		'side':Global.player_side
		})
	
	panel.visible = false
	piece_for_promotion = null
	PossibleMoves.check_for_Check(get_tree().get_nodes_in_group("pieces"))
	pass # Replace with function body.


func _on_bishop_pressed():
	piece_for_promotion.piece_type = 3
	piece_for_promotion.color_pattern = "GOLD"
	piece_for_promotion.update_state()
	piece_for_promotion.color_pattern = Global.player_side.to_upper()
	
	Client.send({
		'type':'promotion',
		'new_pos':PossibleMoves.toPosition(piece_for_promotion.old_position),
		'piece_type':3,
		'side':Global.player_side
		})
	
	panel.visible = false
	piece_for_promotion = null
	PossibleMoves.check_for_Check(get_tree().get_nodes_in_group("pieces"))
	pass # Replace with function body.


func _on_queen_pressed():
	piece_for_promotion.piece_type = 4
	piece_for_promotion.color_pattern = "GOLD"
	piece_for_promotion.update_state()
	piece_for_promotion.color_pattern = Global.player_side.to_upper()
	
	Client.send({
		'type':'promotion',
		'new_pos':PossibleMoves.toPosition(piece_for_promotion.old_position),
		'piece_type':4,
		'side':Global.player_side
		})
	
	panel.visible = false
	piece_for_promotion = null
	PossibleMoves.check_for_Check(get_tree().get_nodes_in_group("pieces"))
	pass # Replace with function body.
