class_name Palette
extends Node


const PIECE_COLOR = {
	"WHITE": ["ffffff", "677d97", "afbfd2", "14182e"],
	"BLACK": ["4c6885", "2b2b45", "3a3f5e", "14182e"],
	"GOLD":  ["ffffff", "e97647", "ffe37d", "1a1227"]
}
