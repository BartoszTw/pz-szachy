extends Camera2D

func _ready():
	pass

func _process(delta):
	var zoom_vec =  Vector2(720.0, 880.0) / get_viewport_rect().size 
	var zoom_val = max(zoom_vec.x, zoom_vec.y)
	zoom = Vector2(zoom_val, zoom_val)
