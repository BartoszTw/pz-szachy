extends Node2D
class_name Piece

onready var area = $Area2D
onready var figure_sprite = $Figure
onready var icon_sprite = $Icon
onready var old_position = global_position

var dragging = false
var click_position = Vector2.ZERO
var click_offset = Vector2.ZERO

var count_moves = 0

export var piece_type = 0
export var orientation = 0		#	0 - przód, 1 - tył

var pos = Vector2.ZERO
var color_pattern = "BLACK"

const origin_point = Vector2(40, -40)

const PIECE_TYPE = {
	0 : "P",			#Pawn
	1 : "R", 			#Rook
	2 : "N",			#kNight
	3 : "B", 			#Bishop
	4 : "Q",			#Queen
	5 : "K"				#King
}

func _ready():
	update_state()

func _physics_process(delta):
	if dragging:
		if not Input.is_action_pressed("click"):
			dragging = false
			global_position = old_position
			pos = old_position
		

func _input(event):
	if Global.player_side == color_pattern.to_lower():
		if event is InputEventMouseMotion and dragging:
			click_offset = old_position - click_position
			set_global_position(click_offset + get_global_mouse_position())

func _on_Area2D_input_event(viewport, event, shape_idx):
	if Global.player_side == color_pattern.to_lower():
		if event is InputEventMouseButton and event.button_index == BUTTON_LEFT:
			#checking possible moves
			if not dragging:
				var this_position = PossibleMoves.toPosition(old_position)
				var this_piece
				var pieces = get_tree().get_nodes_in_group("pieces")
				for piece in pieces:
					if this_position == PossibleMoves.toPosition(piece.old_position):
						this_piece = piece
						break
				var possible_moves = PossibleMoves.possible_moves_for_piece(this_piece, get_tree().get_nodes_in_group("pieces"))
				var castling = PossibleMoves_AM.possible_moves_for_castling(this_piece, get_tree().get_nodes_in_group("pieces"))
				var forbidden_moves = PossibleMoves_AM.impossible_moves_for_piece_AM(this_piece, possible_moves, get_tree().get_nodes_in_group("pieces"))
				possible_moves = PossibleMoves_AM.delete_king_from_array(this_piece, possible_moves, get_tree().get_nodes_in_group("pieces"))

				if castling[0] == Vector2(-1,-1):
					if castling.size() > 1:
						forbidden_moves += [castling[1]]
				elif castling[0] == Vector2(1,-1):
					possible_moves += [castling[1]]
					
				var diff_possible_moves = difference(possible_moves, forbidden_moves)
				for vec in diff_possible_moves:
					GlobalTileMap.overlay.draw_tile(vec.x, vec.y)
				for	vec in forbidden_moves:
					GlobalTileMap.overlay.draw_wrong_tile(vec.x, vec.y)
			click_position = get_global_mouse_position()
			dragging = event.pressed
			if not dragging:
				#hide suggestions
				GlobalTileMap.overlay.hide_tiles()
				SignalConnector.emit_signal("grid_drag_stop", self, global_position, old_position)
				click_position = Vector2.ZERO

static func difference(arr1, arr2):
	var only_in_arr1 = []
	for v in arr1:
		if not (v in arr2):
			only_in_arr1.append(v)
	return only_in_arr1

func _handle_drag(pos):
	if Global.player_side == color_pattern.to_lower():
		if dragging:
			self.set_global_position(pos)

func update_position(pos):
	global_position = pos
	old_position = pos

func update_state():
	figure_sprite.visible = not Global.piece_as_icons
	icon_sprite.visible = Global.piece_as_icons
	set_figure()
	set_color(Palette.PIECE_COLOR[color_pattern])
	
func set_figure():
	figure_sprite.frame_coords = Vector2(piece_type, orientation)
	icon_sprite.frame_coords = Vector2(piece_type, 0)

func set_color(color):
	figure_sprite.material.set_shader_param("piece_color1", Color(color[0]))
	figure_sprite.material.set_shader_param("piece_color2", Color(color[1]))
	figure_sprite.material.set_shader_param("piece_color3", Color(color[2]))
	figure_sprite.material.set_shader_param("piece_color4", Color(color[3]))
	icon_sprite.material.set_shader_param("piece_color1", Color(color[0]))
	icon_sprite.material.set_shader_param("piece_color2", Color(color[1]))
	icon_sprite.material.set_shader_param("piece_color3", Color(color[2]))
	icon_sprite.material.set_shader_param("piece_color4", Color(color[3]))

#func deserialize(info):
#	pos = Vector2(float(str(info)[0]), float(str(info)[1]))
#	counter_type = PIECE_TYPE[str(info)[2]]


#func set_figure():
#	sprite.frame_coords = Vector2(counter_type, orientation)
#	position = pos * 20




