extends Node2D
class_name PossibleMoves

#var is_check = false
#var is_checked = false
#var is_check_mate = false
#var is_stalemate = false

static func possible_moves_for_piece(piece, pieces):
	var result = []
	match piece.piece_type:
		0:
			result = check_for_pawn(piece, toPosition(piece.old_position), pieces)
		1:
			result = check_for_rook(piece, toPosition(piece.old_position), pieces)
		2:
			result = check_for_knight(piece, toPosition(piece.old_position), pieces)
		3:
			result = check_for_bishop(piece, toPosition(piece.old_position), pieces)
		4:
			result = check_for_queen(piece, toPosition(piece.old_position), pieces)
		5:
			result = check_for_king(piece, toPosition(piece.old_position), pieces)
		_:
			return []
	return result

static func check_for_Check(pieces):
	var all_own_possible_moves = []
	var own_pieces = []
	var enemy_king = null
	var enemy_king_pos = null
	
	var is_already_checking = Global.is_check
	for piece in pieces:
		if Global.player_side == piece.color_pattern.to_lower():
			own_pieces.append(piece)
		elif piece.piece_type == 5:
			enemy_king = piece
			enemy_king_pos = toPosition(piece.old_position)
	for piece in own_pieces:
		all_own_possible_moves += possible_moves_for_piece(piece, pieces)
	print("all_own_possible_moves: ", all_own_possible_moves)
	if enemy_king_pos in all_own_possible_moves:
		Global.is_check = true
	else:
		Global.is_check = false
			
	if(Global.is_check and not is_already_checking):
		print("Check!!!")
		Interface.status_panel_label.text = "Check"
		Interface.status_panel.visible = true
		
		# possible checkmate
		
		Client.send({
			'type':'check',
			'side':Global.player_side
			})
	elif(Global.is_check and is_already_checking):
		print("Check Mate!!! WIN!!!")
		Interface.status_panel_label.text = "Mate, You win"
		Interface.status_panel.visible = true
		Global.game_status = true
		Client.send({
			'type':'win',
			'side':Global.player_side
			})
	elif(not Global.is_check and is_already_checking):
		Interface.status_panel.visible = false
		print("Out of check!")
	
	return

static func check_for_Stalemate(pieces):
	var possible_moves = []
	var own_king = null
	var own_king_pos = null
	var enemy_king = null
	var enemy_king_pos = null
	
	for piece in pieces:
		if piece.piece_type == 5 and Global.player_side == piece.color_pattern.to_lower():
			own_king = piece
			own_king_pos = toPosition(piece.old_position)
		elif piece.piece_type == 5 and Global.player_side != piece.color_pattern.to_lower():
			enemy_king = piece
			enemy_king_pos = toPosition(piece.old_position)
	
	for piece in pieces:
		possible_moves += possible_moves_for_piece(piece, pieces)
	
	for possible_move in possible_moves:
		if possible_move != own_king_pos or possible_move != enemy_king_pos:
			Global.is_stalemate = true
			print("Stalemate!!!")	
			Client.send({
				'type':'stelemate',
				'side':Global.player_side
				})
	return 

static func promotion(pieces):
	var promotion_piece = null
	var is_promotion = false
	
	for piece in pieces:
		var pos = toPosition(piece.old_position)
		if piece.piece_type == 0 and Global.player_side == piece.color_pattern.to_lower() and pos.y == 0:
			promotion_piece = piece
			break
	
	if promotion_piece != null:		
		Interface.piece_for_promotion = promotion_piece
		Interface.panel.visible = true
		is_promotion = true
		print("Promotion!")
	
	return

static func check_for_pawn(specific_piece, pos, pieces):
	var moves = []
	var moves_count = specific_piece.count_moves
	var move1 = null
	var move2 = null
	# front
	for piece in pieces:
		var other_pos = toPosition(piece.old_position)
		if moves_count==0:
			if pos + Vector2(0, -2) == other_pos:
				move1 = other_pos
		if pos + Vector2(0, -1) == other_pos:
				move2 = other_pos	
	# diagonal	
	for piece in pieces:
		var other_pos = toPosition(piece.old_position)
		var p_t = piece.piece_type
		if ((pos+Vector2(-1,-1) == other_pos and piece.color_pattern != specific_piece.color_pattern) or 
			(pos+Vector2(1,-1) == other_pos and piece.color_pattern != specific_piece.color_pattern)):
			moves.append(other_pos)
	# is Vector2
	if typeof(move1) != 5 and moves_count == 0:
		moves.append(pos + Vector2(0, -2))
	if typeof(move2) != 5:
		moves.append(pos + Vector2(0, -1))
		
	return moves
	
static func check_for_rook(specific_piece, pos, pieces):
	var moves = []
	var u = range(1, pos.y+1, 1)
	var r = range(1, 7-pos.x+1, 1)
	var d = range(1, 7-pos.y+1, 1)
	var l = range(1, pos.x+1, 1)
	
	moves = moves + drift_move(specific_piece, pos, pieces, u, 0)
	moves = moves + drift_move(specific_piece, pos, pieces, r, 2)
	moves = moves + drift_move(specific_piece, pos, pieces, d, 4)
	moves = moves + drift_move(specific_piece, pos, pieces, l, 6)
	
	# castling
	var king = null
	for piece in pieces:
		if (Vector2(4,7) == toPosition(piece.old_position) and 
			piece.piece_type == 5 and
			specific_piece.color_pattern == piece.color_pattern and
			piece.count_moves == 0):
			king == piece
	
	if specific_piece.count_moves == 0 and king != null:
		pass
	else:
		return moves
	
	var diff = toPosition(king.old_position).x - pos.x
	var move_list
	var is_piece = true
	if diff < 0:	# short
		move_list = range(1, abs(diff), 1)
	else:	# long
		move_list = range(1, diff, 1)
	for index in move_list:
		is_piece = false
		for piece in pieces:
			var other_pos = toPosition(piece.old_position)
			var this_pos 
			if diff < 0:
				this_pos = Vector2(pos.x - index, pos.y)
			else:
				this_pos = Vector2(pos.x + index, pos.y)
			if(this_pos == other_pos):
				is_piece = true
				break
		if(is_piece == true):
			break
		
	if is_piece == false:
		moves.append(toPosition(king.old_position))
		
	return moves

	
static func check_for_bishop(specific_piece, pos, pieces):
	var moves = []
	var ur = range(1, min(pos.y, 7-pos.x)+1, 1)
	var dr = range(1, min(7-pos.y, 7-pos.x)+1, 1)
	var dl = range(1, min(7-pos.y, pos.x)+1, 1)
	var ul = range(1, min(pos.y, pos.x)+1, 1)

	moves = moves + drift_move(specific_piece, pos, pieces, ur, 1)
	moves = moves + drift_move(specific_piece, pos, pieces, dr, 3)
	moves = moves + drift_move(specific_piece, pos, pieces, dl, 5)
	moves = moves + drift_move(specific_piece, pos, pieces, ul, 7)

	return moves

static func check_for_knight(specific_piece, pos, pieces):
	var moves = []
	var move_list = [Vector2(1,2),Vector2(2,1),Vector2(2,-1),Vector2(1,-2),
				Vector2(-1,-2),Vector2(-2,-1),Vector2(-2,1),Vector2(-1,2)]
	
	moves = moves + jump_move(specific_piece, pos, pieces, move_list)

	return moves
	
static func check_for_queen(specific_piece, pos, pieces):
	var moves = []
	var ur = range(1, min(pos.y, 7-pos.x)+1, 1)
	var dr = range(1, min(7-pos.y, 7-pos.x)+1, 1)
	var dl = range(1, min(7-pos.y, pos.x)+1, 1)
	var ul = range(1, min(pos.y, pos.x)+1, 1)
	var u = range(1, pos.y+1, 1)
	var d = range(1, 7-pos.y+1, 1)
	var r = range(1, 7-pos.x+1, 1)
	var l = range(1, pos.x+1, 1)

	moves = moves + drift_move(specific_piece, pos, pieces, u, 0)
	moves = moves + drift_move(specific_piece, pos, pieces, ur, 1)
	moves = moves + drift_move(specific_piece, pos, pieces, r, 2)
	moves = moves + drift_move(specific_piece, pos, pieces, dr, 3)
	moves = moves + drift_move(specific_piece, pos, pieces, d, 4)
	moves = moves + drift_move(specific_piece, pos, pieces, dl, 5)
	moves = moves + drift_move(specific_piece, pos, pieces, l, 6)
	moves = moves + drift_move(specific_piece, pos, pieces, ul, 7)

	return moves

	
static func check_for_king(specific_piece, pos, pieces):
	var moves = []
	var move_list = [Vector2(0,1),Vector2(1,1),Vector2(1,0),Vector2(1,-1),
				Vector2(0,-1),Vector2(-1,-1),Vector2(-1,0),Vector2(-1,1)]
				
	moves = moves + jump_move(specific_piece, pos, pieces, move_list)

	return moves

static func drift_move(specific_piece, pos, pieces, move_range, side):
	var moves = []
	for index in move_range:
		var is_piece = false
		var this_pos 
		# 0, 1,  2, 3,  4, 5,  6, 7
		# u, ur, r, dr, d, dl, l, ul
		match side:
			0:
				this_pos = Vector2(pos.x, pos.y - index)
			1:
				this_pos = Vector2(pos.x + index, pos.y - index)
			2:
				this_pos = Vector2(pos.x + index, pos.y)
			3:
				this_pos = Vector2(pos.x + index, pos.y + index)
			4:
				this_pos = Vector2(pos.x, pos.y + index)
			5:
				this_pos = Vector2(pos.x - index, pos.y + index)
			6:
				this_pos = Vector2(pos.x - index, pos.y)
			7:
				this_pos = Vector2(pos.x - index, pos.y - index)
		for piece in pieces:
			var other_pos = toPosition(piece.old_position)
			if(this_pos == other_pos):
				if (specific_piece.color_pattern == piece.color_pattern): #ally piece
					is_piece = true
				else: #enemy piece
					moves.append(this_pos)
					is_piece = true
		if(is_piece):
			break
		else:
			moves.append(this_pos)	
	return moves
	
static func jump_move(specific_piece, pos, pieces, move_list):
	var moves = []
	for shift in move_list:
		var is_piece = false
		var this_pos = pos + shift
		if(this_pos.x < 0 or this_pos.x > 7 or this_pos.y < 0 or this_pos.y > 7):
			continue
		for piece in pieces:
			var other_pos = toPosition(piece.old_position)
			if(this_pos == other_pos and specific_piece.color_pattern == piece.color_pattern):
				is_piece = true
				break
		if(is_piece == false):
			moves.append(this_pos)	
	return moves
	
static func toPosition(coord :Vector2):
	var old_p = coord/80 
	var old_position = Vector2(int(old_p.x), int(old_p.y))
	return old_position
