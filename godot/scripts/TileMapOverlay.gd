extends TileMap

func draw_tile(x, y):
	set_cell(x, y, 0) 

func draw_wrong_tile(x, y):
	set_cell(x, y, 1) 

func hide_tiles():
	for cell in get_used_cells():
		set_cellv(cell, INVALID_CELL)

