extends TileMap

onready var overlay = $TileMapOverlay

var move_effect = preload("res://scenes/Particle.tscn")

func _ready():
	SignalConnector.connect("grid_drag_stop", self, "_handle_drag_stop")

func _input(event):
	if event is InputEventMouseMotion:
		var mouse_pos : Vector2 = get_global_mouse_position()
		var coord : Vector2 = map_to_world(world_to_map(mouse_pos))
		SignalConnector.emit_signal("grid_position", coord)

static func string_to_vector2(string := "") -> Vector2:
	if string:
		var new_string: String = string
		new_string.erase(0, 1)
		new_string.erase(new_string.length() - 1, 1)
		var array: Array = new_string.split(",")
		
		return Vector2(array[0], array[1])
		
	return Vector2.ZERO

func _enemy_move(enemy_move):
	
	var enemy_side = enemy_move["side"]
	if Global.player_side == enemy_side:
		return
	
	print(enemy_move)
	var enemy_type = enemy_move["type"]	
	
	if(enemy_type == "check" or
		enemy_type == "win" or
		enemy_type == "stalemate" or
		enemy_type == "promotion"):
			
		match enemy_type:
			"check":
				Global.is_checked = true
				Interface.status_panel_label.text = "Enemy is checking You"
				Interface.status_panel.visible = true
				return
			"win":
				Interface.status_panel_label.text = "Mate, You lose"
				Interface.status_panel.visible = true
				Global.game_status = true
				return
			"stalemate":
				Interface.status_panel_label.text = "Stalemate, draw"
				Interface.status_panel.visible = true
				Global.game_status = true
				return
			"promotion":
				var pos_of_promotion_piece = Vector2(string_to_vector2(str2var(enemy_move["new_pos"])))
				var piece_type = enemy_move["piece_type"]
				pos_of_promotion_piece.y = 7-pos_of_promotion_piece.y
				var pieces = get_tree().get_nodes_in_group("pieces")
				for piece in pieces:
					var pos = PossibleMoves.toPosition(piece.old_position)
					if pos == pos_of_promotion_piece:
						piece.piece_type = piece_type
						piece.update_state()
						var effect = move_effect.instance()
						add_child(effect)
						effect.global_position = (pos * 80) + Vector2(40, 40)
						break
				return
		return
	
	# was able to make a move so stopped being checked
	Global.is_check = false
	Interface.status_panel.visible = false
	
	match enemy_type:
		"piece_move":
			var enemy_old_move = Vector2(string_to_vector2(str2var(enemy_move["old_pos"])))
			var enemy_new_move = Vector2(string_to_vector2(str2var(enemy_move["new_pos"])))
			enemy_old_move.y = 7-enemy_old_move.y
			enemy_new_move.y = 7-enemy_new_move.y
			
			#print(enemy_old_move)
			#print(enemy_new_move)
			#print("spece")
			var pieces = get_tree().get_nodes_in_group("pieces")
			for piece in pieces:
				var old_p = piece.global_position/80 
				var old_position_grid = Vector2(int(old_p.x), int(old_p.y))
				if enemy_old_move == old_position_grid:
					var effect = move_effect.instance()
					add_child(effect)
					effect.global_position = (enemy_new_move * 80) + Vector2(40, 40)
					piece.update_position((enemy_new_move * 80) + Vector2(40, 40))
					piece.count_moves += 1
					break
					
		"piece_move_capture":
			var enemy_old_move = Vector2(string_to_vector2(str2var(enemy_move["new_pos"])))
			var enemy_new_move = Vector2(string_to_vector2(str2var(enemy_move["old_pos"])))
			enemy_old_move.y = 7-enemy_old_move.y
			enemy_new_move.y = 7-enemy_new_move.y
			
			var pieces = get_tree().get_nodes_in_group("pieces")
			var capturing_piece
			var captured_piece 
			
			for piece in pieces:
				var old_p = piece.global_position/80 
				var old_position_grid = Vector2(int(old_p.x), int(old_p.y))
				if enemy_old_move == old_position_grid:
					captured_piece = piece
					break
			for piece in pieces:
				var new_p = piece.global_position/80 
				var new_position_grid = Vector2(int(new_p.x), int(new_p.y))
				if enemy_new_move == new_position_grid:
					capturing_piece = piece
					break
			captured_piece.queue_free()
			var effect = move_effect.instance()
			add_child(effect)
			effect.global_position = (enemy_old_move * 80) + Vector2(40, 40)
			capturing_piece.update_position((enemy_old_move * 80) + Vector2(40, 40))
			capturing_piece.count_moves += 1
				
		"piece_move_castling":
			var king 
			var rook
			var king_new_pos
			var rook_new_pos
			
			var pieces = get_tree().get_nodes_in_group("pieces")
			for piece in pieces:
				var new_p = piece.global_position/80 
				var new_position_grid = Vector2(int(new_p.x), int(new_p.y))
				if new_position_grid == Vector2(4,0):
					king = piece
					break

			if enemy_move["type_of_castling"] == "short":
				for piece in pieces:
					var new_p = piece.global_position/80 
					var new_position_grid = Vector2(int(new_p.x), int(new_p.y))
					if new_position_grid == Vector2(7,0):
						rook = piece
						king_new_pos = Vector2(6,0)
						rook_new_pos = Vector2(5,0)
						break				
			else: #  enemy_move["type_of_castling"] == "long":
				for piece in pieces:
					var new_p = piece.global_position/80 
					var new_position_grid = Vector2(int(new_p.x), int(new_p.y))
					if new_position_grid == Vector2(0,0):
						rook = piece
						king_new_pos = Vector2(2,0)
						rook_new_pos = Vector2(3,0)
						break
			var effect1 = move_effect.instance()
			var effect2 = move_effect.instance()
			add_child(effect1)
			add_child(effect2)
			effect1.global_position = king.global_position
			effect2.global_position = rook.global_position
			king.update_position((king_new_pos * 80) + Vector2(40, 40))
			rook.update_position((rook_new_pos * 80) + Vector2(40, 40))
			king.count_moves += 1
			rook.count_moves += 1
			#'type':                -> "piece_move_castling"
			#'type_of_castling':    -> "short" albo "long"
			#'side':                -> "white" albo "black"
		_:
			return
	Global.turn += 1
	return
	
const PIECE_MOVE_TYPE = {
	0 : "piece_move",
	1 : "piece_move_capture",
	2 : "piece_move_castling"
	}

	
func _handle_drag_stop(piece, new_pos, old_pos):
	var possible_and_forbidden_moves =  possible_and_forbidden_moves(piece, PossibleMoves_AM.toPosition(new_pos))
	
	print(possible_and_forbidden_moves)
	
	var old_pos_grid = PossibleMoves_AM.toPosition(old_pos)
	var new_pos_grid = PossibleMoves_AM.toPosition(new_pos)
	var coord = world_to_map(new_pos)
	var cell = get_cell(coord.x, coord.y)	
	
	if cell == INVALID_CELL or Global.game_status == true:
		print("invalid cell")
		piece.set_global_position(old_pos)
	if Global.player_side == "white" and Global.turn % 2 == 1:
		print("not your turn")
		piece.set_global_position(old_pos)
		return 
	if Global.player_side == "black" and Global.turn % 2 == 0:
		print("not your turn")
		piece.set_global_position(old_pos)
		return 
	if old_pos_grid == coord:
		print("same place")
		piece.set_global_position(old_pos)
		return
		
	var pieces = get_tree().get_nodes_in_group("pieces")
	
	# [possible_moves, forbidden_moves, diff_possible_moves]
	#var possible_and_forbidden_moves =  possible_and_forbidden_moves(piece, new_pos_grid)
	
	print(possible_and_forbidden_moves[0],
		possible_and_forbidden_moves[1],
		possible_and_forbidden_moves[2])
	
	var only_possible_moves = possible_and_forbidden_moves[2]
	
	if not new_pos_grid in only_possible_moves:
		print("wrong place")
		piece.set_global_position(old_pos)
		return
	
	var operation = PIECE_MOVE_TYPE[0]
	
	var captured_piece = null
	
	for specific_piece in pieces:
		var skip = false
		var specific_piece_pos = PossibleMoves_AM.toPosition(specific_piece.old_position)
		for possible_move in only_possible_moves:
			if (specific_piece_pos == possible_move and 
			piece.color_pattern != specific_piece.color_pattern and
			new_pos_grid == specific_piece_pos):
				skip = true
				captured_piece = specific_piece
				operation = PIECE_MOVE_TYPE[1]
				break
			elif (specific_piece_pos==possible_move and 
				piece.color_pattern == specific_piece.color_pattern and
				specific_piece.piece_type == 5 and
				new_pos_grid == specific_piece_pos):
				skip = true
				operation = PIECE_MOVE_TYPE[2]
				break
		if(skip == true):
			break
	
	
	match operation:
		"piece_move":
			piece_move_operation(piece, coord, old_pos_grid)
		"piece_move_capture":
			piece_move_capture_operation(piece, coord, old_pos_grid, captured_piece)
		"piece_move_castling":
			piece_move_castling_operation(piece, coord, old_pos_grid)
		_:
			piece.set_global_position(old_pos)
			return
		
	pieces = get_tree().get_nodes_in_group("pieces")
	
	Global.is_checked = false
	Interface.status_panel.visible = false
	
	PossibleMoves.promotion(pieces)
	PossibleMoves.check_for_Check(pieces)
	Global.turn += 1
	
	return


####################################################################################################
	
	var old_p = old_pos/80 
	var old_position_grid = Vector2(int(old_p.x), int(old_p.y))
	#var coord = world_to_map(new_pos)
	#var cell = get_cell(coord.x, coord.y)	
	if cell == INVALID_CELL or Global.game_status == true:
		piece.set_global_position(old_pos)
	elif cell in range(2):
		if validate_move(piece.piece_type, coord, old_position_grid, piece.count_moves, piece):
			#efekt
			var effect = move_effect.instance()
			add_child(effect)
			effect.global_position = (coord * 80) + Vector2(40, 40)
			#pionek
			piece.update_position((coord * 80) + Vector2(40, 40))
			#1) chess piece, 2) old pos, 3) new pos				
			Client.send({
				'type':'piece_move',
				'piece_type': Piece.PIECE_TYPE[piece.piece_type],
				'old_pos':str(old_position_grid).replace(" ",""),
				'new_pos':str(coord).replace(" ",""),
				'side':Global.player_side})	
				
			Global.is_checked = false
			Interface.status_panel.visible = false
			
			PossibleMoves.promotion(get_tree().get_nodes_in_group("pieces"))
			PossibleMoves.check_for_Check(get_tree().get_nodes_in_group("pieces"))
			Global.turn += 1
		elif capture_move(old_position_grid, coord, piece) == true:
			return
		else:
			#exception if the piece is on the same place as new move
			#false or
			#[king ,king_new_pos, rook, rook_new_pos]
			var is_castling_or_list
			if piece.piece_type == 1 and piece.count_moves == 0:
				is_castling_or_list = PieceMoves.check_for_castling(piece, get_tree().get_nodes_in_group("pieces"))
				if typeof(is_castling_or_list) != TYPE_ARRAY:
					piece.set_global_position(old_pos)
				else:
					if Global.player_side == "white" and Global.turn % 2 == 1:
						print("not your turn")
						piece.set_global_position(old_pos)
						return
					if Global.player_side == "black" and Global.turn % 2 == 0:
						print("not your turn")
						piece.set_global_position(old_pos)
						return
					
					var effect1 = move_effect.instance()
					var effect2 = move_effect.instance()
					add_child(effect1)
					add_child(effect2)
					effect1.global_position = (is_castling_or_list[1] * 80) + Vector2(40, 40)
					effect2.global_position = (is_castling_or_list[3] * 80) + Vector2(40, 40)
					is_castling_or_list[0].update_position((is_castling_or_list[1] * 80) + Vector2(40, 40))
					is_castling_or_list[2].update_position((is_castling_or_list[3] * 80) + Vector2(40, 40))
					is_castling_or_list[0].count_moves += 1
					is_castling_or_list[2].count_moves += 1
					
					var type_of_castling
					# king position after castling to determin if it was long or short castling
					# long
					if(is_castling_or_list[1].x == 2):
						type_of_castling = "long"
					# short	-> is_castling_or_list[1].x == 6
					else:
						type_of_castling = "short"
					Client.send({
						'type':'piece_move_castling',
						'type_of_castling':type_of_castling,
						'side':Global.player_side})				
					Global.turn += 1
					
					Global.is_checked = false
					Interface.status_panel.visible = false
					
					PossibleMoves.promotion(get_tree().get_nodes_in_group("pieces"))
					PossibleMoves.check_for_Check(get_tree().get_nodes_in_group("pieces"))
					return
			piece.set_global_position(old_pos)		
	else:
		piece.set_global_position(old_pos)

func validate_move(piece_type, coord, old_coord, count_moves, specific_piece):
	if Global.player_side == "white" and Global.turn % 2 == 1:
		print("not your turn")
		return false
	if Global.player_side == "black" and Global.turn % 2 == 0:
		print("not your turn")
		return false
	if old_coord == coord:
		print("same place")
		return false
	var real_coord = (coord * 80) + Vector2(40, 40)
	var pieces = get_tree().get_nodes_in_group("pieces")
	if not PieceMoves.validate(piece_type, coord, count_moves, specific_piece, pieces): return false
	for piece in pieces: 
		if piece.global_position == real_coord : return false
	specific_piece.count_moves += 1
	return true

func capture_move(old_pos, new_pos, specific_piece):
	var pieces = get_tree().get_nodes_in_group("pieces")
	var resoult = PieceCapture.validate(old_pos, new_pos, specific_piece, pieces)
	print("resoult of capturing ", resoult)
	if typeof(resoult) == TYPE_BOOL:
		specific_piece.set_global_position(old_pos)
		return false
	else:				
		Client.send({
			'type':'piece_move_capture',
			'piece_type':Piece.PIECE_TYPE[specific_piece.piece_type],
			'piece_type_captured':Piece.PIECE_TYPE[resoult.piece_type],
			'new_pos':str(new_pos).replace(" ",""),
			'old_pos':str(old_pos).replace(" ",""),
			'side':Global.player_side})
				
		Global.turn += 1
		
		resoult.queue_free()
		specific_piece.update_position((new_pos * 80) + Vector2(40, 40))
		var effect = move_effect.instance()
		add_child(effect)
		effect.global_position = (new_pos * 80) + Vector2(40, 40)
		
		Global.is_checked = false
		Interface.status_panel.visible = false
		
		PossibleMoves.promotion(get_tree().get_nodes_in_group("pieces"))
		PossibleMoves.check_for_Check(get_tree().get_nodes_in_group("pieces"))
		specific_piece.count_moves += 1
		return true
	return false

##########################################################################################

func piece_move_operation(piece, coord_new, old_pos_grid):
	print(piece, coord_new, old_pos_grid)
	var effect = move_effect.instance()
	add_child(effect)
	effect.global_position = (coord_new * 80) + Vector2(40, 40)
	piece.update_position((coord_new * 80) + Vector2(40, 40))
	Client.send({
		'type':'piece_move',
		'piece_type': Piece.PIECE_TYPE[piece.piece_type],
		'old_pos':str(old_pos_grid).replace(" ",""),
		'new_pos':str(coord_new).replace(" ",""),
		'side':Global.player_side})	
	piece.count_moves += 1
	return

func piece_move_capture_operation(piece, coord_new, old_pos_grid, captured_piece):
	Client.send({
		'type':'piece_move_capture',
		'piece_type':Piece.PIECE_TYPE[piece.piece_type],
		'piece_type_captured':Piece.PIECE_TYPE[captured_piece.piece_type],
		'new_pos':str(coord_new).replace(" ",""),
		'old_pos':str(old_pos_grid).replace(" ",""),
		'side':Global.player_side})
	
	captured_piece.queue_free()
	piece.update_position((coord_new * 80) + Vector2(40, 40))
	var effect = move_effect.instance()
	add_child(effect)
	effect.global_position = (coord_new * 80) + Vector2(40, 40)
	piece.count_moves += 1
	return

func piece_move_castling_operation(rook, coord, old_pos_grid):
	var pieces = get_tree().get_nodes_in_group("pieces")
	var rook_pos = PossibleMoves_AM.toPosition(rook.old_position)
	var king
	var king_pos
	
	for specific_piece in pieces:
		king_pos = PossibleMoves_AM.toPosition(specific_piece.old_position)
		if king_pos == Vector2(4,7):
			king = specific_piece
			break

	var type_of_castling
	var king_new_pos
	var rook_new_pos
	# king position after castling to determin if it was long or short castling
	# long
	if(abs(rook_pos.x - king_pos.x) == 4):
		type_of_castling = "long"
		king_new_pos = Vector2(2,7)
		rook_new_pos = Vector2(3,7)
	else:
		type_of_castling = "short"
		king_new_pos = Vector2(6,7)
		rook_new_pos = Vector2(5,7)

	var effect1 = move_effect.instance()
	var effect2 = move_effect.instance()
	add_child(effect1)
	add_child(effect2)
	effect1.global_position = (king_new_pos * 80) + Vector2(40, 40)
	effect2.global_position = (rook_new_pos * 80) + Vector2(40, 40)
	#może pomylone (dwie następne linie)
	king.update_position((king_new_pos * 80) + Vector2(40, 40))
	rook.update_position((rook_new_pos * 80) + Vector2(40, 40))
	king.count_moves += 1
	rook.count_moves += 1

	Client.send({
		'type':'piece_move_castling',
		'type_of_castling':type_of_castling,
		'side':Global.player_side})				


func possible_and_forbidden_moves(this_piece, future_move):
	var pieces = get_tree().get_nodes_in_group("pieces")
	
	var possible_moves = PossibleMoves.possible_moves_for_piece(this_piece, pieces)
	var castling =  PossibleMoves_AM.possible_moves_for_castling(this_piece, pieces)
	var forbidden_moves = PossibleMoves_AM.impossible_moves_for_piece_AM(this_piece, possible_moves, pieces)
	possible_moves = PossibleMoves_AM.delete_king_from_array(this_piece, possible_moves, get_tree().get_nodes_in_group("pieces"))
	
	if castling[0] == Vector2(-1,-1):
		if castling.size() > 1:
			forbidden_moves += [castling[1]]
	elif castling[0] == Vector2(1,-1):
		possible_moves += [castling[1]]
	
	var diff_possible_moves = Piece.difference(possible_moves, forbidden_moves)
	
	return [possible_moves, forbidden_moves, diff_possible_moves]

func receive_move(pos: Vector2):
	print(pos)




