extends Node

var is_check = false
var is_checked = false
var is_check_mate = false
var is_stalemate = false

# for locking game
var game_status = false

var piece_as_icons = false
# "black" / "white", null -> spactator
#var player_side = null;
# DELETE_ME _
var player_side = "white";
var turn = 0

func _init():
	load_player_site()
	print("Piece view mode as icons:", piece_as_icons)
	print("Player site:", player_side)

func load_player_site():
	if OS.has_feature('JavaScript'):
		player_side = JavaScript.eval("parent.playerSide")

func _process(delta):
	if not get_tree().get_root().transparent_bg:
		get_tree().get_root().transparent_bg = true
		set_process(false)
