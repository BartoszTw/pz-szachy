extends Node2D
class_name PossibleMoves_AM

# AM === after_move

# funkcja będzie wykonywana przed każdym wybraniem nowej pozycji:
# ruch, zbicie, roszada, promocja


# czerwony kolor podświetlenia  = spowodowanie szachu własnego króla
# niebieski kolor podświetlenia = szach króla przeciwnika

# rozważyć roszadę

static func impossible_moves_for_piece_AM(piece, future_move, pieces):
	if piece.piece_type == 5:
		return impossible_moves_for_king_AM(piece, future_move, pieces)
	var piece_current_position = piece.old_position
	var own_king
	var own_king_pos
	
	for piece in pieces:
		if Global.player_side == piece.color_pattern.to_lower() and piece.piece_type == 5:
			own_king = piece
			own_king_pos = toPosition(own_king.old_position)
			break
	
	var possible_pieces = []
	for enemy_piece in pieces:
		if Global.player_side != enemy_piece.color_pattern.to_lower():
			possible_pieces.append(enemy_piece)
			
	var forbidden_moves = []
	
	# czy po ruchu własny król będzie szachowany
	for f_moves in future_move:
		var skip = false
		piece.old_position = toGlobalPosition(f_moves)
		for enemy_piece in possible_pieces:
			var list_of_enemy_moves = []
			if enemy_piece.piece_type == 0:
				list_of_enemy_moves = check_for_enemy_pawn(enemy_piece, toPosition(enemy_piece.old_position), pieces)
			else:
				list_of_enemy_moves = PossibleMoves.possible_moves_for_piece(enemy_piece, pieces)
			
			# can capture possiblely checking figure
			if toPosition(enemy_piece.old_position) == toPosition(piece.old_position):
				continue
			# własny król jest na liście ruchów przeciwnika
			if list_of_enemy_moves.find(own_king_pos) != -1: # oraz nie jest figurą która sprawdzamy
				forbidden_moves.append(toPosition(piece.old_position))
				skip = true
				break
		if(skip == true):
			continue		
	piece.old_position = piece_current_position
	
	print("FORBIDDEN  MOVES: ", forbidden_moves)
	
	return forbidden_moves

static func impossible_moves_for_king_AM(king, future_move, pieces):
	
	var own_king_pos = king.old_position
	
	var possible_pieces = []
	for enemy_piece in pieces:
		if Global.player_side != enemy_piece.color_pattern.to_lower():
			possible_pieces.append(enemy_piece)
	
	var forbidden_moves = []
	
	for f_moves in future_move:
		var skip = false
		king.old_position = toGlobalPosition(f_moves)
		print(f_moves)
		for enemy_piece in possible_pieces:
			var list_of_enemy_moves = []
			if enemy_piece.piece_type == 0:
				list_of_enemy_moves = check_for_enemy_pawn(enemy_piece, toPosition(enemy_piece.old_position), pieces)
			else:
				list_of_enemy_moves = PossibleMoves.possible_moves_for_piece(enemy_piece, pieces)
			# can capture possiblely checking figure
			if toPosition(enemy_piece.old_position) == toPosition(king.old_position):
				continue
			# własny król jest na liście ruchów przeciwnika
			
			#print("ruchy prerzeciwników: ", list_of_enemy_moves)
			#print("vec:", toPosition(king.old_position))
			if list_of_enemy_moves.find(toPosition(king.old_position)) != -1: # oraz nie jest figurą która sprawdzamy
				forbidden_moves.append(toPosition(king.old_position))
				skip = true
				break
		if(skip == true):
			continue		
	king.old_position = own_king_pos

	return forbidden_moves

static func possible_moves_for_castling(rook, pieces):
	if(rook.count_moves > 0):
		return [Vector2(-1,-1)]
	var rook_old_pos = rook.old_position
	var result = [Vector2(-1,-1)]
	if rook.piece_type != 1:
		return result
	var rook_pos = toPosition(rook.old_position)
	var king
	var king_pos
	
	for specific_piece in pieces:
		king_pos = toPosition(specific_piece.old_position)
		if king_pos == Vector2(4,7):
			king = specific_piece
			break

	var own_king_pos = king.old_position

	var type_of_castling
	var king_new_pos
	var rook_new_pos
	var position_beetween_rook_and_king
	
	if abs(rook_pos.x - king_pos.x) == 4:
		type_of_castling = "long"
		king_new_pos = Vector2(2,7)
		rook_new_pos = Vector2(3,7)
		position_beetween_rook_and_king = range(1, 4, 1)
	else:
		type_of_castling = "short"
		king_new_pos = Vector2(6,7)
		rook_new_pos = Vector2(5,7)
		position_beetween_rook_and_king = range(1, 3, 1)
	# no pieces between rook and king
	for shift in position_beetween_rook_and_king:
		var piece_after_shift = Vector2(4,7)
		if type_of_castling == "long":
			piece_after_shift.x -= shift 
		else: #type_of_castling = "short"
			piece_after_shift.x += shift 
		for piece in pieces:
			var piece_pos = toPosition(piece.old_position)
			if piece_after_shift == piece_pos:
				return result
	#king.old_position = toGlobalPosition(king_new_pos)
	rook.old_position = toGlobalPosition(rook_new_pos)
	
	result = impossible_moves_for_king_AM(king, [king_new_pos], pieces)
	
	king.old_position = own_king_pos
	rook.old_position = rook_old_pos
	
	if result.size() == 0:
		return [Vector2(1,-1), Vector2(4,7)] #can do castling
	else:
		return [Vector2(-1,-1), Vector2(4,7)] #can't do castling

static func delete_king_from_array(piece, array, pieces):
	var king = null
	for specific_piece in pieces:
		if (specific_piece.piece_type == 5 and 
		specific_piece.color_pattern != piece.color_pattern):
			king = specific_piece
	if king == null:
		return array
	var king_pos = toPosition(king.old_position)
	if king_pos in array:
		array.erase(king_pos)
	return array

static func check_for_enemy_pawn(specific_piece, pos, pieces):
	var moves = []
	var moves_count = specific_piece.count_moves
	var move1 = null
	var move2 = null
	# front
	for piece in pieces:
		var other_pos = toPosition(piece.old_position)
		if moves_count==0:
			if pos + Vector2(0, 2) == other_pos:
				move1 = other_pos
		if pos + Vector2(0, 1) == other_pos:
				move2 = other_pos	
	# diagonal	
	for piece in pieces:
		var other_pos = toPosition(piece.old_position)
		var p_t = piece.piece_type
		if ((pos+Vector2(-1,1) == other_pos and piece.color_pattern != specific_piece.color_pattern) or 
			(pos+Vector2(1,1) == other_pos and piece.color_pattern != specific_piece.color_pattern)):
			moves.append(other_pos)
	# is Vector2
	if typeof(move1) != 5 and moves_count == 0:
		moves.append(pos + Vector2(0, 2))
	if typeof(move2) != 5:
		moves.append(pos + Vector2(0, 1))
		
	return moves 

static func toPosition(coord :Vector2):
	var old_p = coord/80 
	var old_position = Vector2(int(old_p.x), int(old_p.y))
	return old_position

static func toGlobalPosition(pos):
	return (pos * 80) + Vector2(40, 40)
