extends Node
class_name PieceMoves

static func validate(type: int, new_coord: Vector2, count_moves: int, specific_piece, pieces):
	var possible_moves = false
	var old_p = specific_piece.old_position/80 
	var old_position = Vector2(int(old_p.x), int(old_p.y))
	
	print("move: ", old_position, " ", new_coord)
	
	match type:
		0: possible_moves = pawn(new_coord, old_position, count_moves, pieces)
		1: possible_moves = rook(new_coord, old_position, count_moves, pieces)
		2: possible_moves = knight(new_coord, old_position, count_moves, pieces)
		3: possible_moves = bishop(new_coord, old_position, count_moves, pieces)
		4: possible_moves = queen(new_coord, old_position, count_moves, pieces)
		5: possible_moves = king(new_coord, old_position, count_moves, pieces)
	return possible_moves
	
"""
const PIECE_TYPE = {
	"P": 0,				#Pawn
	"R": 1,				#Rook
	"B": 2,				#Bishop
	"N": 3,				#kNight
	"Q": 4,				#Queen
	"K": 5				#King
}
"""

static func pawn(new_position, old_position, count_moves, pieces):
	#print(count_moves)
	if count_moves >= 0 and (old_position - Vector2(0, 1) == new_position): return true
	elif count_moves == 0 and (old_position - Vector2(0, 2) == new_position): 
		for piece in pieces: 
			if toPosition(piece.old_position) - Vector2(0,1) == new_position:
				return false				
		return true
	
	return false

static func rook(new_position, old_position, count_moves, pieces):
	if new_position.x == old_position.x or new_position.y == old_position.y:
		var chess_p = []
		for piece in pieces:
			var piece_p_g = toPosition(piece.old_position)
			if (piece_p_g.x == old_position.x or piece_p_g.y == old_position.y) and not (piece_p_g == old_position):
				chess_p.append(piece_p_g)
		var difference = new_position - old_position
		if difference.x != 0:
			for piece in chess_p:
				if ((piece.x >= new_position.x and piece.x <= old_position.x) or (piece.x <= new_position.x and piece.x >= old_position.x)) and (piece.y == old_position.y):
					return false
		else:
			for piece in chess_p:
				if ((piece.y >= new_position.y and piece.y <= old_position.y) or (piece.y <= new_position.y and piece.y >= old_position.y)) and (piece.x == old_position.x):
					return false	
		return true	
	return false

static func bishop(new_position, old_position, count_moves, pieces):
	#is it even vaiable moove at all - capability of piece
	if abs(new_position.x - old_position.x) == abs(new_position.y - old_position.y):
		var x_range
		var y_range
		var vec_range = []
		
		if new_position.x - old_position.x > 0.0:
			x_range = range(old_position.x, new_position.x)
		else:
			x_range = range(old_position.x, new_position.x, -1)
		if new_position.y - old_position.y > 0.0:
			y_range = range(old_position.y, new_position.y)
		else:
			y_range = range(old_position.y, new_position.y, -1)
		
		for i in x_range.size():
			vec_range.append(Vector2(x_range[i], y_range[i]))

		for piece in pieces:
			var piece_p_g = toPosition(piece.old_position)
			for range_val in vec_range:
				if(piece_p_g == range_val and not piece_p_g == old_position):
					return false			
		return true				
	return false

static func knight(new_position, old_position, count_moves, pieces):
	var move_list = [Vector2(1,2),Vector2(2,1),Vector2(2,-1),Vector2(1,-2),
				Vector2(-1,-2),Vector2(-2,-1),Vector2(-2,1),Vector2(-1,2)]
	for shift in move_list:
		if(old_position + shift == new_position):
			for piece in pieces:
				var piece_p_g = toPosition(piece.old_position)
				if(piece_p_g == new_position):
					return false
			return true
	return false

static func queen(new_position, old_position, count_moves, pieces):
	if (new_position.x == old_position.x or new_position.y == old_position.y) or (abs(new_position.x - old_position.x) == abs(new_position.y - old_position.y)):
		# rook-like move
		if new_position.x == old_position.x or new_position.y == old_position.y:
			var chess_p = []
			for piece in pieces:
				var piece_p_g = toPosition(piece.old_position)
				if (piece_p_g.x == old_position.x or piece_p_g.y == old_position.y) and not (piece_p_g == old_position):
					chess_p.append(piece_p_g)
			var difference = new_position - old_position
			if difference.x != 0:
				for piece in chess_p:
					if ((piece.x >= new_position.x and piece.x <= old_position.x) or (piece.x <= new_position.x and piece.x >= old_position.x)) and (piece.y == old_position.y):
						return false
			else:
				for piece in chess_p:
					if ((piece.y >= new_position.y and piece.y <= old_position.y) or (piece.y <= new_position.y and piece.y >= old_position.y)) and (piece.x == old_position.x):
						return false	
			return true	
			
		#bishop-like move
		if abs(new_position.x - old_position.x) == abs(new_position.y - old_position.y):
			var x_range
			var y_range
			var vec_range = []
			
			if new_position.x - old_position.x > 0.0:
				x_range = range(old_position.x, new_position.x)
			else:
				x_range = range(old_position.x, new_position.x, -1)
			if new_position.y - old_position.y > 0.0:
				y_range = range(old_position.y, new_position.y)
			else:
				y_range = range(old_position.y, new_position.y, -1)
			
			for i in x_range.size():
				vec_range.append(Vector2(x_range[i], y_range[i]))

			for piece in pieces:
				var piece_p_g = toPosition(piece.old_position)
				for range_val in vec_range:
					if(piece_p_g == range_val and not piece_p_g == old_position):
						return false			
			return true
	return false

static func king(new_position, old_position, count_moves, pieces):
	var move_list = [Vector2(0,1),Vector2(1,1),Vector2(1,0),Vector2(1,-1),
				Vector2(0,-1),Vector2(-1,-1),Vector2(-1,0),Vector2(-1,1)]
	for shift in move_list:
		if(old_position + shift == new_position):
			for piece in pieces:
				var piece_p_g = toPosition(piece.old_position)
				if(piece_p_g == new_position):
					return false
			return true
	return false
	
"""
static func unique(coord):
	pass
"""

static func check_for_castling(specific_piece, pieces):
	#(4,7) pozycja króla
	var king = null
	for piece in pieces:
		if piece.count_moves == 0 and piece.piece_type == 5 and specific_piece.color_pattern == piece.color_pattern:
			king = piece
			break
	if king == null:
		return false
		
	var old_rook_position = toPosition(specific_piece.old_position)
	var king_pos = toPosition(king.old_position)
	var x_range
	var vec_range = []
	var king_potential_new_pos
	var rook_potential_new_pos
	if old_rook_position.x > king_pos.x:
		x_range = range(king_pos.x+1, old_rook_position.x)
		king_potential_new_pos = Vector2(king_pos.x+2, king_pos.y)
		rook_potential_new_pos = Vector2(old_rook_position.x-2, king_pos.y)
	else:
		x_range = range(old_rook_position.x+1, king_pos.x)
		king_potential_new_pos = Vector2(king_pos.x-2, king_pos.y)
		rook_potential_new_pos = Vector2(old_rook_position.x+3, king_pos.y)
	
	for i in x_range.size():
		vec_range.append(Vector2(x_range[i], king_pos.y))
		
	for piece in pieces:
		var piece_p_g = toPosition(piece.old_position)
		for range_val in vec_range:
			if piece_p_g == range_val:
				return false
		
	var new_position_list = [king ,king_potential_new_pos, specific_piece, rook_potential_new_pos]	
	#print(new_position_list)	
	king.set_global_position(king_potential_new_pos)
	specific_piece.set_global_position(rook_potential_new_pos)
	return new_position_list


static func toPosition(coord :Vector2):
	var old_p = coord/80 
	var old_position = Vector2(int(old_p.x), int(old_p.y))
	return old_position
