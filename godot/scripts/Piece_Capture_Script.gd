extends Node
class_name PieceCapture

#piece_p_g = piece position grid

static func validate(old_pos, new_pos, specific_piece, pieces):
	var possible_moves = false
	var type = specific_piece.piece_type
	if(old_pos == new_pos):
		return false
	match type:
		0: possible_moves = pawn(old_pos, new_pos, specific_piece, pieces)
		1: possible_moves = rook(old_pos, new_pos, specific_piece, pieces)
		2: possible_moves = knight(old_pos, new_pos, specific_piece, pieces)
		3: possible_moves = bishop(old_pos, new_pos, specific_piece, pieces)
		4: possible_moves = queen(old_pos, new_pos, specific_piece, pieces)
		5: possible_moves = king(old_pos, new_pos, specific_piece, pieces)
	return possible_moves
	
static func pawn(old_pos, new_pos, specific_piece, pieces):
	#no En passant ;__;
	var move_list = [Vector2(-1,-1),Vector2(1,-1)]
	if (old_pos+move_list[0] == new_pos) or (old_pos+move_list[1] == new_pos):
		for piece in pieces:
			var piece_p_g = toPosition(piece.old_position)
			if(piece_p_g == new_pos) and (piece.color_pattern != specific_piece.color_pattern) and (piece.piece_type != 5):
				return piece
	else:
		return false
	return false

static func rook(old_pos, new_pos, specific_piece, pieces):
	if new_pos.x == old_pos.x or new_pos.y == old_pos.y:
		var chess_p = []
		var resoult = false
		for piece in pieces:
			var piece_p_g = toPosition(piece.old_position)
			if (piece_p_g.x == old_pos.x or piece_p_g.y == old_pos.y) and not (piece_p_g == old_pos):
				chess_p.append(piece_p_g)
			if (piece_p_g == new_pos and specific_piece != piece and specific_piece 
				and piece.piece_type != 5 and specific_piece.color_pattern != piece.color_pattern):
				resoult = piece
		var difference = new_pos - old_pos
		if difference.x != 0:
			for piece in chess_p:
				if ((piece.x > new_pos.x and piece.x < old_pos.x) or (piece.x < new_pos.x and piece.x > old_pos.x)) and (piece.y == old_pos.y):
					return false
		else:
			for piece in chess_p:
				if ((piece.y > new_pos.y and piece.y < old_pos.y) or (piece.y < new_pos.y and piece.y > old_pos.y)) and (piece.x == old_pos.x):
					return false	
		if typeof(resoult) == TYPE_BOOL:
			return false
		else:
			return resoult	
	return false
	
static func knight(old_position, new_position, specific_piece, pieces):
	var resoult = false
	var move_list = [Vector2(1,2),Vector2(2,1),Vector2(2,-1),Vector2(1,-2),
				Vector2(-1,-2),Vector2(-2,-1),Vector2(-2,1),Vector2(-1,2)]
	for shift in move_list:
		if(old_position + shift == new_position):
			for piece in pieces:
				var piece_p_g = toPosition(piece.old_position)
				if (piece_p_g == new_position and specific_piece != piece and specific_piece 
					and piece.piece_type != 5 and specific_piece.color_pattern != piece.color_pattern):
					resoult = piece
			return resoult
	return false

static func bishop(old_position, new_position, specific_piece, pieces):
	if abs(new_position.x - old_position.x) == abs(new_position.y - old_position.y):
		var x_range
		var y_range
		var vec_range = []
		var resoult = false
		
		if new_position.x - old_position.x > 0.0:
			x_range = range(old_position.x, new_position.x)
		else:
			x_range = range(old_position.x, new_position.x, -1)
		if new_position.y - old_position.y > 0.0:
			y_range = range(old_position.y, new_position.y)
		else:
			y_range = range(old_position.y, new_position.y, -1)
		
		for i in x_range.size():
			vec_range.append(Vector2(x_range[i], y_range[i]))

		for piece in pieces:
			var piece_p_g = toPosition(piece.old_position)
			if (piece_p_g == new_position and specific_piece != piece and specific_piece 
				and piece.piece_type != 5 and specific_piece.color_pattern != piece.color_pattern):
				resoult = piece
			for range_val in vec_range:
				if(piece_p_g == range_val and not piece_p_g == old_position):
					return false			
		return resoult				
	return false

static func queen(old_pos, new_pos, specific_piece, pieces):
	if (new_pos.x == old_pos.x or new_pos.y == old_pos.y) or (abs(new_pos.x - old_pos.x) == abs(new_pos.y - old_pos.y)):
		if new_pos.x == old_pos.x or new_pos.y == old_pos.y:
			var chess_p = []
			var resoult = false
			for piece in pieces:
				var piece_p_g = toPosition(piece.old_position)
				if (piece_p_g.x == old_pos.x or piece_p_g.y == old_pos.y) and not (piece_p_g == old_pos):
					chess_p.append(piece_p_g)
				if (piece_p_g == new_pos and specific_piece != piece and specific_piece 
					and piece.piece_type != 5 and specific_piece.color_pattern != piece.color_pattern):
					resoult = piece
			var difference = new_pos - old_pos
			if difference.x != 0:
				for piece in chess_p:
					if ((piece.x > new_pos.x and piece.x < old_pos.x) or (piece.x < new_pos.x and piece.x > old_pos.x)) and (piece.y == old_pos.y):
						return false
			else:
				for piece in chess_p:
					if ((piece.y > new_pos.y and piece.y < old_pos.y) or (piece.y < new_pos.y and piece.y > old_pos.y)) and (piece.x == old_pos.x):
						return false	
			if typeof(resoult) == TYPE_BOOL:
				return false
			else:
				return resoult	
				
		if abs(new_pos.x - old_pos.x) == abs(new_pos.y - old_pos.y):
			var x_range
			var y_range
			var vec_range = []
			var resoult = false
			
			if new_pos.x - old_pos.x > 0.0:
				x_range = range(old_pos.x, new_pos.x)
			else:
				x_range = range(old_pos.x, new_pos.x, -1)
			if new_pos.y - old_pos.y > 0.0:
				y_range = range(old_pos.y, new_pos.y)
			else:
				y_range = range(old_pos.y, new_pos.y, -1)
			
			for i in x_range.size():
				vec_range.append(Vector2(x_range[i], y_range[i]))

			for piece in pieces:
				var piece_p_g = toPosition(piece.old_position)
				if (piece_p_g == new_pos and specific_piece != piece and specific_piece 
					and piece.piece_type != 5 and specific_piece.color_pattern != piece.color_pattern):
					resoult = piece
				for range_val in vec_range:
					if(piece_p_g == range_val and not piece_p_g == old_pos):
						return false			
			return resoult				
	return false

static func king(old_pos, new_pos, specific_piece, pieces):
	var resoult = false
	var move_list = [Vector2(0,1),Vector2(1,1),Vector2(1,0),Vector2(1,-1),
				Vector2(0,-1),Vector2(-1,-1),Vector2(-1,0),Vector2(-1,1)]
	for shift in move_list:
		if(old_pos + shift == new_pos):
			for piece in pieces:
				var piece_p_g = toPosition(piece.old_position)
				if (piece_p_g == new_pos and specific_piece != piece and specific_piece 
					and piece.piece_type != 5 and specific_piece.color_pattern != piece.color_pattern):
					resoult = piece
			return resoult
	return false

static func toPosition(coord :Vector2):
	var old_p = coord/80 
	var old_position = Vector2(int(old_p.x), int(old_p.y))
	return old_position
