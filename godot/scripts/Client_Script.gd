extends Node

var _client = WebSocketClient.new();

export var HOST_URL = "localhost:8000"

func _ready():
	_client.connect("connection_established", self, "_connection_established")
	_client.connect("connection_error", self, "_connection_error")
	_client.connect("connection_closed", self, "_connection_closed")
	_client.connect("data_received", self, "_data_received")
	
	var err = _client.connect_to_url(websocket_url_validation(get_websocket()))
	if err != OK:
		print("WS client connect to url error: ", err)
		set_process(false)

func _connection_established(protocol):
	print("WS client connection established with protocol: ", protocol)
	# Send WS message example
	# send({"type":"message","message":"GODOT"})

func _connection_closed(was_clean_close):
	print("WS client connection closed: ", was_clean_close)

func _connection_error():
	print("WS client connection error!")

func _data_received():
	var result = JSON.parse(_client.get_peer(1).get_packet().get_string_from_utf8()).result
	GlobalTileMap._enemy_move(result)
	print("WS client data received: ", result)
	#get_tree().get_root().get_node("Main").receive_move(Vector2(0,0))

func _process(delta):
	_client.poll()

func send(message):
	_client.get_peer(1).put_packet(JSON.print(message).to_utf8())

func get_websocket():
	if OS.has_feature('JavaScript'):
		return JavaScript.eval("'ws://" + HOST_URL + "/ws/gameroom/' + parent.window.location.pathname.split('/').pop() + '/'")
	return null
	
func websocket_url_validation(websocket_url):
	var _websocket_url = str(websocket_url)
	if _websocket_url.right(len(_websocket_url) - 1) != "/":
		return _websocket_url + "/"
	return _websocket_url
