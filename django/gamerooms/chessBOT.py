import chess
import random

def decode_move(old_pos_json, new_pos_json, side, type_of_castling_json):
    if old_pos_json==None or new_pos_json==None:
        if side=="w":
            if type_of_castling_json == "short": return "e1h1"
            else: return "e1a1"
        else:
            if type_of_castling_json == "short": return "e8h8"
            else: return "e8a8"

    old_pos = f"{old_pos_json.translate({ord(i): None for i in '(), '})}"
    new_pos = f"{new_pos_json.translate({ord(i): None for i in '(), '})}"

    if side=="b": return f"{chr(97+int(old_pos_json[1]))}{int(old_pos[1])+1}{chr(97+int(new_pos_json[1]))}{int(new_pos[1])+1}"
    else: return f"{chr(97+int(old_pos[0]))}{abs(int(old_pos[1])-8)}{chr(97+int(new_pos[0]))}{abs(int(new_pos[1])-8)}"

def code_move(move, side):
    if side == "b":
        old = f"({ord(move[0])-97},{int(move[1])-1})"
        new = f"({ord(move[2])-97},{int(move[3])-1})"
        return old, new
    else:
        old = f"({ord(move[0])-97},{8-int(move[1])})"
        new = f"({ord(move[2])-97},{8-int(move[3])})"
        return old, new

def get_capture_piece(board_old_param, board_new_param):
    board_old = board_old_param.translate({ord(i): None for i in '012345678/- '}).upper()
    board_new = board_new_param.translate({ord(i): None for i in '012345678/- '}).upper()

    if len(board_old)!=len(board_new):
        if board_old.count("P") != board_new.count("P"): return "P"
        elif board_old.count("N") != board_new.count("N"): return "N"
        elif board_old.count("B") != board_new.count("B"): return "B"
        elif board_old.count("R") != board_new.count("R"): return "R"
        elif board_old.count("Q") != board_new.count("Q"): return "Q"
    else: return None

def get_piece(board, move, side):
    row = int(move[3]); col = int(move[1])
    new_board = board.split("/")
    if side == "b": new_board.reverse()
    piece_row = list(new_board[row])

    for i in piece_row:
        if col==0:
            piece = i
            break
        elif i.isdigit(): col -= int(i)
        else: col -= 1

    return piece.upper()

def calculate_move(board, bot_color):
    fen = board.fen().translate({ord(i): None for i in '012345678/- '})
    turn = fen[-1]; fen = fen[:-1]; score = 0
    
    if bot_color=="b": w=-1;b=1
    else: w=1;b=-1
    
    score+=((b*fen.count("p"))+(w*fen.count("P")))
    score+=((b*fen.count("n"))+(w*fen.count("N")))*3
    score+=((b*fen.count("b"))+(w*fen.count("B")))*3
    score+=((b*fen.count("r"))+(w*fen.count("R")))*5
    score+=((b*fen.count("q"))+(w*fen.count("Q")))*10

    if board.is_checkmate():
        if bot_color==turn: return score-100
        else: return score+100
    
    if board.is_stalemate():
        if score<0: return score+50
        else: return score-50

    return score

def move_tree(board, maxdeeph, bot_color, deeph=1):
    score = calculate_move(board, bot_color)
    if maxdeeph+1!=deeph:
        score_move = []
        for move in board.legal_moves:
            board.push(chess.Move.from_uci(str(move)))
            score_move.append(move_tree(board, maxdeeph, bot_color, deeph+1))
            board.pop()
        
        if len(score_move)>0:
            return score + max(score_move)
        else: return score
    else: return 0

def get_best_move(chess_board, color, deeph):
    board = chess.Board(f"{chess_board} {color}")
    max_score = None
    move_list = []
    test_list = []
    
    for move in board.legal_moves:
        board.push(chess.Move.from_uci(str(move)))
        score = move_tree(board,deeph,color)

        if max_score==None:
            max_score = score
            move_list.append(str(move))
        if max_score<score:
            move_list.clear()
            move_list.append(str(move))
            max_score = score
        elif max_score==score:
            move_list.append(str(move))

        test_list.append(f"{move}: {score}")
        board.pop()

    rand_id = random.randint(0, len(move_list)-1)

    old_board = board.fen().split(" ")
    board.push(chess.Move.from_uci(str(move_list[rand_id])))

    new_board = board.fen().split(" ")
    old_pos, new_pos = code_move(move_list[rand_id], color)
    capture_piece = get_capture_piece(old_board[0], new_board[0])

    piece = get_piece(new_board[0], new_pos, color)

    return old_pos, new_pos, new_board[0], piece, capture_piece

def get_actual_board(board_fen, old_pos_json, new_pos_json, side, type_of_castling_json):
    move = decode_move(old_pos_json, new_pos_json, side, type_of_castling_json)

    board = chess.Board(f"{board_fen} {side}")
    board.push(chess.Move.from_uci(move))
    r_board = board.fen().split(" ")

    return r_board[0]