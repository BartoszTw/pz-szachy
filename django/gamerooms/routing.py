# gamerooms/routing.py
from django.urls import re_path

from . import consumers

websocket_urlpatterns = [
    re_path(r"ws/gameroom/(?P<pk>\w+)/$", consumers.GameroomsConsumer.as_asgi()),
]