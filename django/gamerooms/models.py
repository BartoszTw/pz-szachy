from django.db import models
from users.models import CustomUser as User
from django.db.models.fields import TextField

class Gameroom(models.Model):
    name = models.CharField(max_length=255)
    description = TextField(null=True, blank=True)
    created_by = models.CharField(max_length=50)
    usersInGameroom = models.ManyToManyField(User)
    whiteChessPlayer = models.ForeignKey(User, on_delete=models.SET_NULL, related_name='whiteChessPlayer',null=True, blank=True)
    blackChessPlayer = models.ForeignKey(User, on_delete=models.SET_NULL, related_name='blackChessPlayer',null=True, blank=True)
    gameIsActive = models.BooleanField(default=True)
    board_fen = models.CharField(max_length=100, default="rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR", null=True, blank=True)
    with_bot = models.BooleanField(null=True, blank=True)

    def __str__(self):
        return f"{ self.name }"

class GameStats(models.Model):
    winner_color = models.CharField(max_length=10, null=True, blank=True)
    winner_rank_diff = models.FloatField(null=True, blank=True, default=0)
    loser_rank_diff = models.FloatField(null=True, blank=True, default=0)
    gameroom = models.OneToOneField(Gameroom, on_delete=models.CASCADE)

    def save(self, *args, **kwargs):
        self.winner_rank_diff = round(self.winner_rank_diff, 1)
        self.loser_rank_diff = round(self.loser_rank_diff, 1)

        super(GameStats, self).save(*args, **kwargs)


class ChatMessage(models.Model):
    content = models.CharField(max_length=100)
    timestamp = models.CharField(max_length=7)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    roomID = models.ForeignKey('Gameroom', on_delete=models.CASCADE)

    def str(self):
        return "{} {}".format(self.timestamp, self.user)

class ChessMove(models.Model):
    old_pos = models.CharField(max_length=4, null=True, blank=True)
    new_pos = models.CharField(max_length=4, null=True, blank=True)
    chessPieceIcon = models.ImageField(null=True, blank=True)
    piece_type = models.CharField(max_length=10, null=True, blank=True)
    piece_type_captured = models.CharField(max_length=10, null=True, blank=True)
    type_of_castling = models.CharField(max_length=10, null=True, blank=True)
    side = models.CharField(max_length=10, blank=True, null=True)

    gameroom = models.ForeignKey(Gameroom, on_delete=models.CASCADE)

    def __str__(self):
        return "("+self.gameroom.name+")"

class GameMode(models.Model):
    name = models.CharField(max_length=20, unique=True)
    time = models.TimeField(blank=True, null=True)
    chessPlaces = models.TextField(null=True)
    createdBy = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True)

    def __str__(self):
        return self.name








