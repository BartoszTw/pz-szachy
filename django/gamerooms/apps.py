from django.apps import AppConfig


class GameroomsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'gamerooms'

    def ready(self):
        import gamerooms.signals