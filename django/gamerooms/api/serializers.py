from rest_framework import serializers
from gamerooms.models import Gameroom, ChatMessage, ChessMove, GameMode, GameStats
from users.api.serializer import UserSerializer

class GameStatsSerializer(serializers.ModelSerializer):
    class Meta:
        model = GameStats
        fields = "__all__"

class GameroomSerializer(serializers.ModelSerializer):
    created_by = serializers.StringRelatedField(read_only=True)
    usersInGameroom = UserSerializer(many=True, read_only=True)
    white = UserSerializer(read_only=True, source="whiteChessPlayer")
    black = UserSerializer(read_only=True, source="blackChessPlayer")
    gamestats = GameStatsSerializer(read_only=True)
    
    class Meta:
        model = Gameroom
        fields = "__all__"
        

        def validate(self, data):
            if data["description"] == data["created_by"]:
                raise serializers.ValidationError("opis nie może mieć nazwy twórcy")
            return data

        def validate_description(self, value):
            if len(value) < 10:
                raise serializers.ValidationError("Opis musi mieć przynajmniej 10 znakow")
            return 
        
        def validate_blackChessPlayer(self, data):
            print(data)
            if data["whiteChessPlayer"] == data["blackChessPlayer"]:
                raise serializers.ValidationError("gracze muszą mieć inne kolory figur")
            return data
        

class ChessmoveSerializer(serializers.ModelSerializer):
    class Meta:
        model = ChessMove
        fields = "__all__"
        extra_kwargs = {
            'gameroom': {'read_only': True}
        }

class GameModeSerializer(serializers.ModelSerializer):
    class Meta:
        model = GameMode
        fields = "__all__"
        

