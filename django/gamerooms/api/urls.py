from django.urls import path, include
from rest_framework import routers
from gamerooms.api.views import (GameroomListCreateAPIView, GameroomDetailAPIView,
                                 ChessmoveListCreateAPIView, ChessmoveCreateAPIView,
                                 JoinGameroomAPIView, QuitGameroomAPIView, SetChessColor,
                                 GameroomListWithoutPlayersRoom, GameModeListCreateApiView,
                                 GameModeUserListApiView, GameModeGetDetailApiView,
                                 GameHasEnded)

from gamerooms.api.views import (index,room)


urlpatterns = [
    #gamerooms
    #list
    path("gamerooms/", 
          GameroomListCreateAPIView.as_view(), 
          name="gameroom-list"),

    path("gamerooms/excludePlayersRooms",
          GameroomListWithoutPlayersRoom.as_view(),
          name="gameroom-list-exclude-players-room"
    ),
    #details
    path("gamerooms/<int:pk>/",
          GameroomDetailAPIView.as_view(),
          name="gameroom-detail"),

    #chessmovesList
    path("gamerooms/<int:pk>/chessmoves/", 
          ChessmoveListCreateAPIView.as_view(), 
          name="chessmoves-list"),
    
    path("gamerooms/<int:pk>/chessmoves/<int:chessmove_id>/", 
          ChessmoveCreateAPIView.as_view(), 
          name="chessmoves-details"),

    #join room
    path("gamerooms/<int:pk>/join/",
          JoinGameroomAPIView.as_view(),
          name="join-room"),
    #quit room
    path("gamerooms/<int:pk>/quit/",
          QuitGameroomAPIView.as_view(),
          name="quit-room"),

    #set color
    path("gamerooms/<int:pk>/setColor/",
          SetChessColor.as_view(),
          name="color"),

#     #set white
#     path("gamerooms/<int:pk>/white/",
#           SetWhiteChessColor.as_view(),
#           name="white"),

#     #set colors as null (spectate)
#     path("gamerooms/<int:pk>/spectate/",
#           SpectateGame.as_view(),
#           name="spectate"),

    #GameHasEnded
    path("gamerooms/<int:pk>/endGame/",
          GameHasEnded.as_view(),
          name="endGame"),

    #GameMode
    #list
    path("gamemode/",
          GameModeListCreateApiView.as_view(),
          name="gamemode-list"),

    #player gamemode list
    path("gamemode/createdByUser",
          GameModeUserListApiView.as_view(),
          name="gamemode-list-created_by_user"),

    #details
    path("gamemode/<int:pk>/",
          GameModeGetDetailApiView.as_view(),
          name="gamemode-detail"),
          

]
