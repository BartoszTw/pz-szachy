from rest_framework import permissions
from gamerooms.models import Gameroom

class IsCreatorOrReadOnly(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        if request.method in permissions.SAFE_METHODS:
            return True
        return obj.created_by == request.user.username

class IsInGameroomOrReadOnly(permissions.BasePermission):
    def has_permission(self, request, view):
        if request.method in permissions.SAFE_METHODS:
            return True
        gameroom_id = view.kwargs['pk']
        gameroom = Gameroom.objects.get(id=gameroom_id)
        return request.user in gameroom.usersInGameroom.all()

class IsPlayingOrReadOnly(permissions.BasePermission):
    def has_permission(self, request, view):
        if request.method in permissions.SAFE_METHODS:
            return True
        gameroom_id = view.kwargs['pk']
        gameroom = Gameroom.objects.get(id=gameroom_id)
        return (request.user == gameroom.whiteChessPlayer or request.user == gameroom.blackChessPlayer)



