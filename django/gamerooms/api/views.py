from rest_framework import status, generics
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.generics import get_object_or_404
from rest_framework.viewsets import ModelViewSet
from rest_framework.authentication import TokenAuthentication, SessionAuthentication
from rest_framework.exceptions import APIException
from django.db.models import Count

from gamerooms.models import Gameroom, ChessMove, GameMode, GameStats
from gamerooms.api.serializers import GameroomSerializer, ChessmoveSerializer, GameModeSerializer, GameStatsSerializer
from gamerooms.api.permissions import IsCreatorOrReadOnly, IsPlayingOrReadOnly, IsInGameroomOrReadOnly
from users.models import Profile
from users.api.serializer import ProfileSerializer


from rest_framework.permissions import IsAuthenticated

class GameroomListCreateAPIView(generics.ListAPIView):
    """
        Zwraca queryset pokoi dla zalogowanych użytkowników.
        Zalogowany użytkownik może stworzyć pokój
    """
    queryset = Gameroom.objects.all()
    serializer_class = GameroomSerializer
    lookup_field = "id"
    permission_classes = [IsAuthenticated]
    authentication_classes = [SessionAuthentication, TokenAuthentication]
    def post(self, request, **kwargs):
        serializer = GameroomSerializer(data=request.data)
        request_user = self.request.user
        if serializer.is_valid():
            serializer.save(created_by=request_user.username, usersInGameroom=[request_user,])
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class GameroomListWithoutPlayersRoom(generics.ListAPIView):
    """Lista pokojów z 1 graczem oczekującym, oraz z wyłączeniem tych należących do osoby"""
    queryset = Gameroom.objects.all()
    serializer_class = GameroomSerializer
    permission_classes = [IsAuthenticated]
    authentication_classes = [SessionAuthentication, TokenAuthentication]

    def list(self, request):
        queryset = self.get_queryset().exclude(created_by=request.user.username).annotate(cc=Count('usersInGameroom')).filter(cc=1)
        serializer = GameroomSerializer(queryset, many=True)
        return Response(serializer.data)


class GameroomDetailAPIView(generics.RetrieveUpdateDestroyAPIView):
    """Podstawowy *RUD pokoi dla twórcy pokoju"""

    queryset = Gameroom.objects.all()
    serializer_class = GameroomSerializer
    permission_classes = [IsAuthenticated, IsCreatorOrReadOnly]
    authentication_classes = [SessionAuthentication, TokenAuthentication]



class ChessmoveListCreateAPIView(APIView):
    """
        Zwracanie listy ruchow dla uzytkownikow należących do pokoju
        Zapis nowego ruchu
    """
    permission_classes = [IsAuthenticated, IsPlayingOrReadOnly]
    authentication_classes = [SessionAuthentication, TokenAuthentication]


    def get(self, request, pk):
        gameroom_id = self.kwargs['pk']
        chessmoves = ChessMove.objects.filter(gameroom=gameroom_id)
        serializer = ChessmoveSerializer(chessmoves, many=True)
        return Response(serializer.data)
    
    def post(self, request, **kwargs):
        serializer = ChessmoveSerializer(data=request.data)
        gameroom_id = kwargs.get('pk')
        gameroom = Gameroom.objects.get(id=gameroom_id)
        if serializer.is_valid():
            serializer.save(gameroom=gameroom)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ChessmoveCreateAPIView(generics.RetrieveDestroyAPIView):
    """Podstawowy Retrive i Destroy ruchów dla uzytkownikow w pokoju"""

    queryset = ChessMove.objects.all()
    serializer_class = ChessmoveSerializer
    lookup_url_kwarg = "chessmove_id"
    permission_classes = [IsAuthenticated, IsPlayingOrReadOnly]
    authentication_classes = [SessionAuthentication, TokenAuthentication]

class JoinGameroomAPIView(APIView):
    """dołączanie do pokoju zalogowanego uzytkownika"""
    permission_classes = [IsAuthenticated]
    authentication_classes = [SessionAuthentication, TokenAuthentication]
    def patch(self, request, pk):
        gameroom = get_object_or_404(Gameroom, pk=pk)
        if(self.request.user not in gameroom.usersInGameroom.all()):
            data = {"usersInGameroom": gameroom.usersInGameroom.add(self.request.user)}
        else:
            return Response("Uzytkownik już należy do tego pokoju")
        serializer = GameroomSerializer(gameroom, data=data, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class QuitGameroomAPIView(APIView):
    """wyjście z pokoju zalogowanego uzytkownika"""
    permission_classes = [IsAuthenticated]
    authentication_classes = [SessionAuthentication, TokenAuthentication]
    def patch(self, request, pk):
        gameroom = get_object_or_404(Gameroom, pk=pk)
        if(self.request.user in gameroom.usersInGameroom.all()):
            data = {"usersInGameroom": gameroom.usersInGameroom.remove(self.request.user)}
            if(self.request.user == gameroom.whiteChessPlayer):
                data["whiteChessPlayer"] = None
            elif(self.request.user == gameroom.blackChessPlayer):
                print("HI")
                data["blackChessPlayer"] = None
        else:
            return Response("Uzytkownik nie należy do tego pokoju")
        serializer = GameroomSerializer(gameroom, data=data, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class SetChessColor(APIView):
    """przypisz biale figury"""
    permission_classes = [IsAuthenticated, IsInGameroomOrReadOnly]
    authentication_classes = [SessionAuthentication, TokenAuthentication]
    def patch(self, request, pk):
        gameroom = get_object_or_404(Gameroom, pk=pk)

        if(request.data["assignColor"] == "white"):
            if(gameroom.blackChessPlayer != None and gameroom.blackChessPlayer.id == self.request.user.id):
                data = {"whiteChessPlayer": self.request.user.id, "blackChessPlayer": None}
            elif(gameroom.whiteChessPlayer == None or gameroom.whiteChessPlayer.id == self.request.user.id):
                data = {"whiteChessPlayer": self.request.user.id}
            else:
                return Response("kolor biały jest juz przypisany")
        elif(request.data["assignColor"] == "black"):
            if(gameroom.whiteChessPlayer != None and gameroom.whiteChessPlayer.id == self.request.user.id):
                data = {"blackChessPlayer": self.request.user.id, "whiteChessPlayer": None}
            elif(gameroom.blackChessPlayer == None or gameroom.blackChessPlayer.id == self.request.user.id):
                data = {"blackChessPlayer": self.request.user.id}
            else:
                return Response("kolor czarny jest juz przypisany")
        elif(request.data["assignColor"] == "spectate"):
            if(gameroom.whiteChessPlayer != None and self.request.user.id == gameroom.whiteChessPlayer.id):
                data = {"whiteChessPlayer": None}
            if(gameroom.blackChessPlayer != None and self.request.user.id == gameroom.blackChessPlayer.id):
                data = {"blackChessPlayer": None}
        else:
            return Response("podano niepoprawne dane")
        
        serializer = GameroomSerializer(gameroom, data=data, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

# class SetBlackChessColor(APIView):
#     """przypisz czarne figury"""
#     permission_classes = [IsAuthenticated]
#     authentication_classes = [SessionAuthentication, TokenAuthentication]
#     def patch(self, request, pk):
#         gameroom = get_object_or_404(Gameroom, pk=pk)
#         if(gameroom.whiteChessPlayer != None and gameroom.whiteChessPlayer.id == self.request.user.id):
#             data = {"blackChessPlayer": self.request.user.id, "whiteChessPlayer": None}
#         elif(gameroom.blackChessPlayer == None or gameroom.blackChessPlayer.id == self.request.user.id):
#             data = {"blackChessPlayer": self.request.user.id}
#         else:
#             raise APIException("kolor czarny jest juz przypisany")

        
        
#         serializer = GameroomSerializer(gameroom, data=data, partial=True)
#         if serializer.is_valid():
#             serializer.save()
#             return Response(serializer.data)
#         return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

# class SpectateGame(APIView):
#     """obserwator, jeśli uzytkownik jest którymś z kolorów to przypisuje wartosc null"""
#     permission_classes = [IsAuthenticated]
#     authentication_classes = [SessionAuthentication, TokenAuthentication]
#     def patch(self, request, pk):
#         gameroom = get_object_or_404(Gameroom, pk=pk)

        

#         if(gameroom.whiteChessPlayer != None and self.request.user.id == gameroom.whiteChessPlayer.id):
#             data = {"whiteChessPlayer": None}
#         if(gameroom.blackChessPlayer != None and self.request.user.id == gameroom.blackChessPlayer.id):
#             data = {"blackChessPlayer": None}
            
#         serializer = GameroomSerializer(gameroom, data=data, partial=True)
#         if serializer.is_valid():
#             serializer.save()
#             return Response(serializer.data)
#         return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class GameModeListCreateApiView(generics.ListAPIView):
    """
    Lista trybów gry, umożliwia stworzenie
    """
    queryset = GameMode.objects.all()
    serializer_class = GameModeSerializer
    permission_classes = [IsAuthenticated]
    authentication_classes = [SessionAuthentication, TokenAuthentication]

    def post(self, request, **kwargs):
        serializer = GameModeSerializer(data=request.data)
        serializer.createdBy = self.request.user
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class GameModeGetDetailApiView(generics.ListAPIView):
    serializer_class = GameModeSerializer
    permission_classes = [IsAuthenticated]
    authentication_classes = [SessionAuthentication, TokenAuthentication]

    def get(self, request, pk):
        gamemode_id = self.kwargs['pk']
        gamemode = GameMode.objects.get(id=gamemode_id)
        serializer = GameModeSerializer(gamemode)
        return Response(serializer.data)
   
class GameModeUserListApiView(generics.ListAPIView):
    """Lista trybów gry, należących do konkretnego gracza"""
    queryset = GameMode.objects.all()
    serializer_class = GameModeSerializer
    permission_classes = [IsAuthenticated]
    authentication_classes = [SessionAuthentication, TokenAuthentication]

    def list(self, request):
        queryset = self.get_queryset().filter(createdBy=request.user)
        serializer = GameModeSerializer(queryset, many=True)
        return Response(serializer.data)

class GameHasEnded(APIView):
    """dezaktywacja gry, modyfikacja rankingu graczy"""
    permission_classes = [IsAuthenticated, IsPlayingOrReadOnly]
    authentication_classes = [SessionAuthentication, TokenAuthentication]

    def patch(self, request, pk):
        winner = get_object_or_404(Profile, pk=request.data["winner"])
        loser = get_object_or_404(Profile, pk=request.data["loser"])

        gameroom = get_object_or_404(Gameroom, pk=pk)
        gamestats = get_object_or_404(GameStats, gameroom=gameroom)

        #calculate ELO
        #factor of shifting of rating
        k = 30
        winner_rating = winner.rank
        loser_rating = loser.rank
        
        #winning probability
        winner_p = winner_rating / (winner_rating + loser_rating)
        loser_p = loser_rating / (winner_rating + loser_rating)

        winner_updated = winner_rating + k*(1-winner_p)
        loser_updated = loser_rating + k*(0-loser_p)

        winner_data = {"rank": winner_updated}
        loser_data = {"rank": loser_updated}


        serialize_winner = ProfileSerializer(winner, data=winner_data, partial=True)
        serialize_loser = ProfileSerializer(loser, data=loser_data, partial=True)

        

        #update game stats
        gamestats_data = {
            "winner_color": None,
            "winner_rank_diff": winner_updated - winner_rating,
            "loser_rank_diff": loser_updated - loser_rating
        }

        print(request.data["winner"])
        print(gameroom.blackChessPlayer)

        if(gameroom.whiteChessPlayer != None and request.data["winner"] == gameroom.whiteChessPlayer.id):
            gamestats_data["winner_color"] = "white"
        elif(gameroom.blackChessPlayer != None and request.data["winner"] == gameroom.blackChessPlayer.id):
            gamestats_data["winner_color"] = "black"
        else:
            return Response("bład przy przypisywaniu koloru w gamestats")

        serialzie_gamestats = GameStatsSerializer(gamestats, data=gamestats_data, partial=True)

        #deactivate room 
        gameroom_data = {"gameIsActive": False}
        serialize_gameroom = GameroomSerializer(gameroom, data=gameroom_data, partial=True)

        if serialize_winner.is_valid() and serialize_loser.is_valid() and serialize_gameroom.is_valid() and serialzie_gamestats.is_valid():
            serialize_winner.save()
            serialize_loser.save()
            serialize_gameroom.save()
            serialzie_gamestats.save()
            response_data = {
                "winner": serialize_winner.data["user"],
                "winner_rank": serialize_winner.data["rank"],
                "loser": serialize_loser.data["user"],
                "loser_rank": serialize_loser.data["rank"]
                }

            return Response(serialzie_gamestats.data)
        return Response(serialize_winner.errors, status=status.HTTP_400_BAD_REQUEST)


#tymczasowe
from django.shortcuts import render
def index(request):
    return render(request, "gamerooms/index.html")

def room(request, pk):
    return render(request, "gamerooms/room.html", {"pk": pk})
        