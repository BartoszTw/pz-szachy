from gamerooms.models import Gameroom
from gamerooms.models import GameStats
from django.db.models.signals import post_save
from django.dispatch import receiver

@receiver(post_save, sender=Gameroom)
def create_profile(sender, instance, created, **kwargs):
    print("Created: ", created)

    if created:
        GameStats.objects.create(gameroom=instance)