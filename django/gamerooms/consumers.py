# gameroom/consumers.py
import json

from channels.db import database_sync_to_async
from channels.generic.websocket import AsyncWebsocketConsumer
from .models import Gameroom, ChessMove
from .chessBOT import get_best_move, get_actual_board, code_move

class GameroomsConsumer(AsyncWebsocketConsumer):
    async def connect(self):
        self.pk = self.scope["url_route"]["kwargs"]["pk"]
        self.room_group_name = "gameroom_%s" % self.pk

        # Join room group
        await self.channel_layer.group_add(self.room_group_name, self.channel_name)
        await database_sync_to_async(Gameroom.objects.filter(id=self.pk).update)(board_fen="rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR")

        await self.accept()

    async def disconnect(self, close_code):
        # Leave room group
        await self.channel_layer.group_discard(self.room_group_name, self.channel_name)

    async def websocket_receive(self, message):
        if "text" in str(message):
            await self.receive(message["text"])
        elif "bytes" in str(message):
            await self.receive(str(message["bytes"])[2:-1])

    # Receive message from WebSocket
    async def receive(self, text_data):
        text_data_json = json.loads(text_data)
        
        room = await database_sync_to_async(Gameroom.objects.get)(id=self.pk)
        message_type = text_data_json["type"]

        if message_type == "piece_move" or message_type == "piece_move_capture" or message_type == "piece_move_castling":
            old_pos_json = text_data_json["old_pos"] if "old_pos" in text_data_json else None
            new_pos_json = text_data_json["new_pos"] if "new_pos" in text_data_json else None
            type_of_castling_json = text_data_json["type_of_castling"] if "type_of_castling" in text_data_json else None
            side_json = text_data_json["side"] if "side" in text_data_json else None
            piece_type_json = text_data_json["piece_type"] if "piece_type" in text_data_json else None
            piece_type_captured_json = text_data_json["piece_type_captured"] if "piece_type_captured" in text_data_json else None

            chess_move = ChessMove(
                old_pos = old_pos_json,
                new_pos = new_pos_json,
                chessPieceIcon = None,
                piece_type = piece_type_json,
                piece_type_captured = piece_type_captured_json,
                type_of_castling = type_of_castling_json,
                side = side_json,
                gameroom = room,
            )
            await database_sync_to_async(chess_move.save)()
            await database_sync_to_async(Gameroom.objects.filter(id=self.pk).update)(board_fen=get_actual_board(room.board_fen, old_pos_json, new_pos_json, side_json[0], type_of_castling_json))

            await self.channel_layer.group_send(
                self.room_group_name,
                text_data_json
            )

            if room.with_bot:
                room = await database_sync_to_async(Gameroom.objects.get)(id=self.pk)
                if side_json == "white": side_bot = "black"
                else: side_bot="white"
                old_pos_bot, new_pos_bot, board_bot, piece, capture_piece = get_best_move(room.board_fen, side_bot[0], 2)


                chess_move = ChessMove(
                    old_pos = old_pos_bot,
                    new_pos = new_pos_bot,
                    chessPieceIcon = None,
                    piece_type = piece,
                    piece_type_captured = capture_piece,
                    type_of_castling = None,
                    side = side_bot,
                    gameroom = room,
                )
                await database_sync_to_async(chess_move.save)()
                await database_sync_to_async(Gameroom.objects.filter(id=self.pk).update)(board_fen=board_bot)

                if capture_piece == None:
                    await self.channel_layer.group_send(
                        self.room_group_name,
                        {
                            'type': 'piece_move',
                            'old_pos': old_pos_bot,
                            'new_pos': new_pos_bot,
                            'piece_type': piece,
                            'side': side_bot,
                        })
                else:
                    await self.channel_layer.group_send(
                        self.room_group_name,
                        {
                            'type': 'piece_move_capture',
                            'old_pos': old_pos_bot,
                            'new_pos': new_pos_bot,
                            'piece_type': piece,
                            'piece_type_captured': capture_piece,
                            'side': side_bot,
                        })
        elif message_type == "chat_message" or message_type=="check" or message_type=="win" or message_type=="stelemate" or message_type=="promotion":
            await self.channel_layer.group_send(
                self.room_group_name,
                text_data_json
            )


    async def chat_message(self, event):
        await self.base_message(event)

    async def piece_move(self, event):
        await self.base_message(event)

    async def piece_move_capture(self, event):
        await self.base_message(event)

    async def piece_move_castling(self, event):
        await self.base_message(event)

    async def check(self, event):
        await self.base_message(event)

    async def win(self, event):
        await self.base_message(event)

    async def stelemate(self, event):
        await self.base_message(event)

    async def promotion(self, event):
        await self.base_message(event)

    async def base_message(self, event):
        # Trzeba wykorzystać metody powyżej a nie base_message ponieważ już występują spore różnice
        type = event["type"] if "type" in event else ""
        side = event["side"] if "side" in event else ""
        piece_type = event["piece_type"] if "piece_type" in event else ""
        new_pos = event["new_pos"] if "new_pos" in event else ""
        old_pos = event["old_pos"] if "old_pos" in event else ""
        piece_type_captured = event["piece_type_captured"] if "piece_type_captured" in event else 0
        type_of_castling = event["type_of_castling"] if "type_of_castling" in event else 0
        value = event["value"] if "value" in event else ""
        await self.send(text_data=json.dumps(
            {
                "type": type,
                "piece_type": piece_type,
                "new_pos": new_pos,
                "old_pos": old_pos,
                "piece_type_captured": piece_type_captured,
                "type_of_castling": type_of_castling,
                "side": side,
                "value": value,
            }
        ))