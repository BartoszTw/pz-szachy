from django.contrib import admin
from .models import Gameroom, ChatMessage, ChessMove, GameMode, GameStats

admin.site.register(Gameroom)
admin.site.register(ChatMessage)
admin.site.register(ChessMove)
admin.site.register(GameMode)
admin.site.register(GameStats)

