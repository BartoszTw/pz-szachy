from django.db import models
from users.models import CustomUser as User
from django.db.models.deletion import CASCADE, PROTECT
from django.db.models.fields import TextField
from django.core.validators import MaxValueValidator, MinValueValidator 


class ChatMessage(models.Model):
    content = models.CharField(max_length=100)
    timestamp = models.CharField(max_length=7)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    room = models.ForeignKey('ChatRoom', on_delete=models.CASCADE)

    def str(self):
        return "{} {}".format(self.timestamp, self.user)


class ChatRoom(models.Model):
    name = models.CharField(max_length=255, unique=True)
    description = TextField(null=True, blank=True)

    def str(self):
        return "{}".format(self.name)

