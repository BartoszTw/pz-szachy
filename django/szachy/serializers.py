from rest_framework import serializers
from users.models import CustomUser
from rest_framework.permissions import IsAuthenticated

class UsersGetSerializer(serializers.ModelSerializer):
    permission_classes = [IsAuthenticated]
    class Meta:
        model = CustomUser
        fields = ['username']
        extra_kwargs = {
            'username': {'read_only': True, 'required': True}
        }

class UsersPostSerializer(serializers.ModelSerializer):
    permission_classes = [IsAuthenticated]
    class Meta:
        model = CustomUser
        fields = ['username']
        extra_kwargs = {
            'username': {'read_only': True, 'required': True}
        }