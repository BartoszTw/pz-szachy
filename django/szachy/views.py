from users.models import CustomUser
from . import serializers
from django.contrib.auth import get_user_model
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated

class UsersViewSet(viewsets.ModelViewSet):
    permission_classes = [IsAuthenticated]
    queryset = get_user_model().objects.none()
    http_method_names = ['get','post']

    serializer_classes = {
        'GET': serializers.UsersGetSerializer,
        'POST': serializers.UsersPostSerializer,
    }

    default_serializer_class = serializers.UsersGetSerializer

    def get_queryset(self):
        user = get_user_model().objects
        return user

    def get_serializer_class(self):
        return self.serializer_classes.get(self.action, self.default_serializer_class)

    def list(self, request):
        users = CustomUser.objects.all()
        serializer = serializers.UsersGetSerializer(users, many=True)
        return Response(serializer.data)

    def retrieve(self, request, pk=None):
        user = CustomUser.objects.filter(pk=pk)
        serializer = serializers.UsersGetSerializer(user)
        return Response(serializer.data)