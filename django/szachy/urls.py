from django.contrib import admin
from django.urls import path, include
from rest_framework import routers

from django_registration.backends.one_step.views import RegistrationView
from users.forms import CustomUserForm

from . import views

router = routers.DefaultRouter()
router.register('users', views.UsersViewSet)

urlpatterns = [
    path("api/", include(router.urls)),
    path("api/", include("gamerooms.api.urls")),

    #profile endpoint
    path("api/", include("users.api.urls")),
    path("admin/", admin.site.urls),
    # path("accounts/register/",
    #     RegistrationView.as_view(form_class=CustomUserForm, success_url="/"),
    #     name="django_registration_register"),

    # path("accounts/", include("django.contrib.auth.urls")),

    path("api-auth/", include("rest_framework.urls")),

    path('auth/', include('djoser.urls')),
    path('auth/', include('djoser.urls.authtoken')),
    
]

from django.conf.urls.static import static
from django.conf import settings

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
