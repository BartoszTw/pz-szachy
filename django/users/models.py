from django.contrib.auth.models import AbstractUser
from django.db import models

class CustomUser(AbstractUser):
    pass

class Profile(models.Model):
    user = models.OneToOneField(CustomUser, on_delete=models.CASCADE)
    country = models.CharField(max_length=50, null=True, blank=True)
    city = models.CharField(max_length=50, null=True, blank=True)
    about_me = models.TextField(null=True, blank=True)
    avatar = models.ImageField(null=True, blank=True)
    rank = models.FloatField(default=800)

    def save(self, *args, **kwargs):
        self.rank = round(self.rank, 1)
        super(Profile, self).save(*args, **kwargs)

    def __str__(self):
        return self.user.username

