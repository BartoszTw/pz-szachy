from rest_framework import status, generics
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.generics import get_object_or_404
from rest_framework.authentication import TokenAuthentication, SessionAuthentication


from users.api.serializer import ProfileSerializer, ProfileAvatarSerializer, RankingSerializer
from users.models import Profile

from rest_framework.permissions import IsAuthenticated
from users.api.permissions import IsProfileOwnerOrReadOnly

class ProfileListAPIView(APIView):
    permission_classes = [IsAuthenticated]
    authentication_classes = [SessionAuthentication, TokenAuthentication]

    def get(self, request):
        profiles = Profile.objects.all()
        serializer = ProfileSerializer(profiles, many=True)
        return Response(serializer.data)

class ProfileDetailAPIView(APIView):
    permission_classes = [IsAuthenticated, IsProfileOwnerOrReadOnly]
    authentication_classes = [SessionAuthentication, TokenAuthentication]
    
    def get_object(self, pk):
        profile = get_object_or_404(Profile, pk=pk)
        return profile

    def get(self, request, pk):
        profile = self.get_object(pk)
        serializer = ProfileSerializer(profile)
        return Response(serializer.data)
    
    def put(self, request, pk):
        profile = self.get_object(pk)
        serializer = ProfileSerializer(profile, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class AvatarUpdateView(generics.UpdateAPIView):
    serializer_class = ProfileAvatarSerializer
    permission_classes = [IsAuthenticated]
    authentication_classes = [SessionAuthentication, TokenAuthentication]
    

    def get_object(self):
        profile_object = self.request.user.profile
        return profile_object

class RankingUpdateView(generics.UpdateAPIView):
    serializer_class = RankingSerializer
    permission_classes = [IsAuthenticated]
    authentication_classes = [SessionAuthentication, TokenAuthentication]

    def get_object(self):
        profile_object = self.request.user.profile
        return profile_object
