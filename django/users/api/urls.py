from django.urls import include, path
from rest_framework.routers import DefaultRouter
from users.api.views import (ProfileListAPIView, ProfileDetailAPIView, AvatarUpdateView, RankingUpdateView)

urlpatterns = [
    #List
    path("profile/", ProfileListAPIView.as_view()),
    path("profile/<int:pk>/", ProfileDetailAPIView.as_view()),
    path("avatar/", AvatarUpdateView.as_view(), name="avatar-update"),
    path("profile/<int:pk>/updateRank", RankingUpdateView.as_view()),
]