from rest_framework import serializers
from users.models import Profile, CustomUser

class ProfileSerializer(serializers.ModelSerializer):
    user = serializers.StringRelatedField(read_only=True)
    avatar = serializers.ImageField(read_only=True)

    class Meta:
        model = Profile
        fields = "__all__"

class ProfileAvatarSerializer(serializers.ModelSerializer):
    class Meta:
        model = Profile
        fields = ("avatar",)

    def validate_rank(self, value):
            if value < 0:
                raise serializers.ValidationError("ranking musi być większy niż 0")
            return 

class UserSerializer(serializers.Serializer):
    profile = ProfileSerializer(required=True)
    class Meta:
        model = CustomUser
        fields = ('username', 'email', 'id')

class RankingSerializer(serializers.Serializer):
    class Meta:
        model = CustomUser
        fields = ('rank', )
