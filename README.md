# PZ - Szachy

Serwis internetowy ...

#### Autorzy

`PM` Bartosz Twardziak

`DEV` Cyprian Lazarowski

`DEV` Patryk Morawski

`DEV` Adam Stobiecki

`DEV` Piotr Szpak

## GIT

### Dodanie repozytorium do istniejącego repozytorium lokalnego

```
cd existing_repo
git remote add origin https://gitlab.com/BartoszTw/pz-szachy.git
git branch -M main
git push -uf origin main
```

### Klonowanie repozytorium

```
cd repo
git clone https://gitlab.com/BartoszTw/pz-szachy.git
git checkout -b main
```

## Django

### Superadmin na bazie produkcyjnej

Login: `admin`

Password: `admin`

### Instalacja Django wraz z uruchomieniem

```
py -m ensurepip
python -m pip install -U channels["daphne"]
python.exe -m pip install -r requirements.txt
python manage.py migrate
python manage.py runserver
```

### Migrowanie bazy danych

```
python manage.py makemigrations
python manage.py migrate

# Przydatne gdy po migracji nadal pojawia się błąd informujący że tablica nie istnieje
python manage.py migrate --run-syncdb
```

### Tworzenie super admina

```
python manage.py createsuperuser

# Przydatne przy błędzie "Superuser creation skipped due to not running in a TTY..."
winpty python manage.py createsuperuser
```

### Redis 

Należy zainstalować najnowszą wersję Redisa:
[POBIERZ](https://github.com/tporadowski/redis/releases)

## Vue.js

```
cd vue
npm install
npm run dev
```